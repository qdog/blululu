FROM python:3.9.9-alpine
WORKDIR /app
ENV PIP_NO_CACHE_DIR=1
RUN : \
    && apk add git \
    && pip install micropipenv[toml] \
    &&:

COPY . .

RUN : \
    && micropipenv install --deploy \
    && pip uninstall -y mircropipenv[toml] \
    && apk del git \
    && rm -rf /tmp/* \
    # && find /usr/local/lib/python3.9/site-packages -type f -name "*.so" -delete \
    && rm -rf /root/.cache/* \
    &&:

ENTRYPOINT ["python", "run.py"]
