import logging
import os
from logging.handlers import RotatingFileHandler
from pathlib import Path

from blululu.config import logging_path, logging_level

logging.basicConfig()

os.makedirs(Path(logging_path).parent, exist_ok=True)

log_formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
)

fileHandler = RotatingFileHandler(logging_path, mode='a', maxBytes=1000000 * 25)  # 25 megabyte
fileHandler.setFormatter(log_formatter)
fileHandler.setLevel(logging.DEBUG)

streamHandler = logging.StreamHandler()
streamHandler.setFormatter(log_formatter)
streamHandler.setLevel(logging_level)

logger = logging.getLogger('blululu')
logger.addHandler(fileHandler)
logger.addHandler(streamHandler)

logger.setLevel(logging.DEBUG)
logger.propagate = False
