import enum
from datetime import datetime, timezone, timedelta
from typing import Optional, List, TYPE_CHECKING

from sqlalchemy import DateTime, TypeDecorator, Boolean, false, Integer, JSON
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import declared_attr
from sqlmodel import Field, SQLModel, create_engine, Relationship, Column, Enum

from blululu.config import database_path


class CanYouPlay(enum.Enum):
    YES = enum.auto()
    NO = enum.auto()
    MAYBE = enum.auto()


class Games(enum.Enum):
    Onward = enum.auto()
    EchoArena = enum.auto()
    Pavlov = enum.auto()
    Snapshot = enum.auto()


class BaseSQLModel(SQLModel):
    @declared_attr
    def __tablename__(self) -> str:  # type: ignore
        return self.__qualname__  # type: ignore

    if TYPE_CHECKING:
        def __init_subclass__(cls, table: bool = False, **kwargs): ...


class UTCDateTime(TypeDecorator):
    impl = DateTime
    cache_ok = True

    def process_bind_param(self, value: datetime, dialect):  # type: ignore
        if value is None:
            return

        if isinstance(value, timedelta):
            return value

        if value.tzinfo is None:
            raise ValueError("datetime object should have timezone info")

        return value.astimezone(tz=timezone.utc)

    def process_result_value(self, value: datetime, dialect):
        if value is None:
            return
        return value.replace(tzinfo=timezone.utc)


class UserRegisteredTeamLink(BaseSQLModel, table=True):
    team_id: Optional[str] = Field(default=None, foreign_key='RegisteredTeam.id', primary_key=True)
    user_id: Optional[int] = Field(default=None, foreign_key='User.id', primary_key=True)


class MatchProposalVotes(BaseSQLModel, table=True):
    user_id: int = Field(foreign_key="User.id", primary_key=True)
    match_proposal_id: int = Field(foreign_key="MatchProposal.id", primary_key=True)
    can_play: CanYouPlay = Field(sa_column=Column(Enum(CanYouPlay, nullable=False)))

    user: "User" = Relationship(back_populates="match_proposal_votes")
    match_proposal: "MatchProposal" = Relationship(back_populates="match_proposal_votes")


class TimeSlot(BaseSQLModel, table=True):
    user_id: int = Field(foreign_key="User.id", primary_key=True)
    date: datetime = Field(primary_key=True, sa_column=Column(UTCDateTime(), primary_key=True))
    can_play: CanYouPlay = Field(sa_column=Column(Enum(CanYouPlay), nullable=False,
                                                  server_default=CanYouPlay.NO.name))
    match_id: int = Field(foreign_key="Match.id", nullable=True)  # this is optional

    user: "User" = Relationship(back_populates="timeslots")
    match: Optional["Match"] = Relationship()
    match_proposals: List["MatchProposal"] = Relationship(
        sa_relationship_kwargs=dict(
            back_populates="timeslots",
            uselist=True,
            primaryjoin="and_(MatchProposal.match_id==remote(Match.id),"
                        "remote(Match.team_id)==remote(UserRegisteredTeamLink.team_id),"
                        "UserRegisteredTeamLink.user_id==TimeSlot.user_id,"
                        "MatchProposal.start_date<=TimeSlot.date,"
                        "MatchProposal.end_date>TimeSlot.date)",
            viewonly=True
        ))


class Team(BaseSQLModel, table=True):
    id: str = Field(primary_key=True)
    name: str
    region: str
    game: Games = Field(sa_column=Column(Enum(Games)))

    registered_team: Optional["RegisteredTeam"] = Relationship(back_populates='team')


class User(BaseSQLModel, table=True):
    id: int = Field(primary_key=True)
    timezone: float

    timeslots: list[TimeSlot] = Relationship(back_populates="user")
    match_proposal_votes: list[MatchProposalVotes] = Relationship(back_populates="user")
    #     sa_relationship_kwargs=dict(
    #         remote_side="MatchProposalVotes.user_id",
    #         uselist=True,
    #         # primaryjoin="User.id==foreign(MatchProposalVotes.user_id)"
    #     )
    #     )
    registered_teams: List["RegisteredTeam"] = Relationship(back_populates="users", link_model=UserRegisteredTeamLink)


class RegisteredTeam(BaseSQLModel, table=True):
    id: str = Field(foreign_key="Team.id", primary_key=True)
    discord_guild_id: int = Field(nullable=False)
    discord_role_id: int = Field(nullable=False, sa_column=Column(Integer, unique=True))
    discord_channel_team_general_id: int = Field(nullable=False)
    discord_channel_proposals_id: int = Field(nullable=False)
    discord_channel_discussion_id: int = Field(nullable=False)

    team: Team = Relationship(back_populates='registered_team')
    users: list[User] = Relationship(back_populates="registered_teams", link_model=UserRegisteredTeamLink)
    matches: List["Match"] = Relationship(back_populates="proposerer_team",
                                          sa_relationship_kwargs=dict(
                                              cascade="all,delete-orphan"
                                          ))


class _MatchThread(BaseSQLModel):
    id: int = Field(primary_key=True)
    message_id: int
    match_id: int = Field(foreign_key='Match.id')
    last_heartbeat_message_id: int = Field(nullable=True)


class MatchDiscussionThread(_MatchThread, table=True):
    match: "Match" = Relationship(back_populates='match_discussion_thread')


class MatchProposalThread(_MatchThread, table=True):
    match: "Match" = Relationship(back_populates='match_proposal_thread')


class Match(BaseSQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    team_id: str = Field(foreign_key='RegisteredTeam.id')
    against_team_id: str = Field(foreign_key='Team.id')
    comment: str = Field(nullable=True)
    is_archived: bool = Field(sa_column=Column(Boolean(), server_default=false()))

    proposerer_team: RegisteredTeam = Relationship(back_populates="matches")
    against_team: Team = Relationship(sa_relationship_kwargs=dict(
        primaryjoin='Match.against_team_id==Team.id'
    ))
    match_discussion_thread: MatchDiscussionThread = Relationship(back_populates="match",
                                                                  sa_relationship_kwargs=dict(
                                                                      uselist=False,
                                                                      cascade="all,delete-orphan"
                                                                  ))
    match_proposal_thread: MatchProposalThread = Relationship(back_populates="match",
                                                              sa_relationship_kwargs=dict(
                                                                  uselist=False,
                                                                  cascade="all,delete-orphan"
                                                              ))

    match_proposal_votes: List["MatchProposalVotes"] = Relationship(
        sa_relationship_kwargs=dict(
            uselist=True,
            secondary="MatchProposal",
            viewonly=True,
        )
    )

    match_proposals: List["MatchProposal"] = Relationship(
        back_populates="match",
        sa_relationship_kwargs=dict(
            cascade="all,delete-orphan"
        )
    )


class MatchProposal(BaseSQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    message_id: int
    match_id: int = Field(foreign_key='Match.id')
    is_confirmed: bool
    start_date: datetime = Field(sa_column=Column(UTCDateTime(), nullable=False))
    end_date: datetime = Field(sa_column=Column(UTCDateTime(), nullable=False))
    last_reminder: datetime = Field(sa_column=Column(UTCDateTime(), nullable=True))

    match: Match = Relationship(back_populates="match_proposals")
    timeslots: List["TimeSlot"] = Relationship(
        back_populates="match_proposals",
        sa_relationship_kwargs=dict(
            uselist=True,
            primaryjoin="and_(MatchProposal.match_id==remote(Match.id),"
                        "remote(Match.team_id)==remote(UserRegisteredTeamLink.team_id),"
                        "UserRegisteredTeamLink.user_id==TimeSlot.user_id,"
                        "MatchProposal.start_date<=TimeSlot.date,"
                        "MatchProposal.end_date>TimeSlot.date)",
            viewonly=True))
    match_proposal_votes: list[MatchProposalVotes] = Relationship(
        back_populates="match_proposal",
        sa_relationship_kwargs=dict(
            cascade="all,delete-orphan"
        )
    )


class AdminAnnouncement(BaseSQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    created_at: datetime = Field(sa_column=Column(UTCDateTime(), nullable=False),
                                 default_factory=lambda: datetime.now(timezone.utc))
    sent: bool = Field(sa_column=Column(Boolean(), server_default=false(), nullable=False))
    message: dict = Field(sa_column=Column(JSON(), nullable=False))


engine = create_engine(f"sqlite:///{database_path}")

SQLModel.metadata.create_all(engine)

async_engine = create_async_engine(f"sqlite+aiosqlite:///{database_path}")
