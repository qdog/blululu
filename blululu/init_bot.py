from discord import Intents, Client
from discord_slash import SlashCommand

bot = Client(intents=Intents.default())
slash = SlashCommand(bot, sync_commands=True, delete_from_unused_guilds=True)
