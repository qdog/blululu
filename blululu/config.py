from typing import TypedDict, Optional
import yaml
import os
import shutil

DATA_DIRECTORY = 'data'

if not os.path.exists(os.path.join(DATA_DIRECTORY, 'blululu.yml')):
    shutil.copy('blululu.yml.example', os.path.join(DATA_DIRECTORY, 'blululu.yml'))

with open(os.path.join(DATA_DIRECTORY, "blululu.yml"), "r") as f:
    bot_config = yaml.safe_load(f)

debug_guilds = bot_config.get("guild_ids", None)


class Admin(TypedDict):
    discord_id: int
    email: str


admins: dict[str, Admin] = bot_config["admins"]

log_info = bot_config['log']
logging_path = os.path.join(DATA_DIRECTORY, log_info['path'])
logging_level = log_info['level']

database_info = bot_config['database']
database_path = os.path.join(DATA_DIRECTORY, database_info['path'])

with open('version.txt', 'r') as fp:
    version = fp.read()

data_version: Optional[str]
if os.path.exists('data/version.txt'):
    with open('data/version.txt', 'r') as fp:
        data_version = fp.read()
else:
    data_version = None
