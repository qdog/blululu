import discord
from discord import TextChannel
from discord_slash import SlashCommand

from blululu.config import version, data_version
from blululu.logger import logger


def load_last_changelog() -> discord.Embed:
    with open('CHANGELOG.md', 'r') as fp:
        changelog = fp.read()

    last_changelog = changelog.split('\n# v')[0]
    _, last_changelog = last_changelog.split('\n', 1)
    sections = last_changelog.split('\n\n')
    sections_h: dict[str, list[str]] = dict()
    for s in sections:
        header, lines = s.split('\n', 1)
        header = header[3:]
        sections_h[header] = []
        for row in lines.split('\n'):
            if '*' in row:
                sections_h[header].append(row[2:])

    embed_str = ''

    for sh, sc in sections_h.items():
        embed_str += f"**{sh}**\n"
        embed_str += ''.join([f'- {sli}\n' for sli in sc]) + '\n'

    return discord.Embed(title=f"Version update to {version}", description=embed_str)


def update(slash: SlashCommand, bot: discord.Client):
    embed = load_last_changelog()

    with open('data/version.txt', 'w') as fp:
        fp.write(version)

    @bot.event
    async def on_ready():
        logger.debug('GOT TO READY STATEMENT IN update')
        from blululu.tables import async_engine, RegisteredTeam
        from sqlmodel import select
        from sqlmodel.ext.asyncio.session import AsyncSession
        async with AsyncSession(async_engine) as session:
            registered_teams: list[RegisteredTeam] = (await session.exec(
                select(RegisteredTeam)  # type: ignore
            )).all()
            logger.debug(f"{registered_teams=}")
            for rt in registered_teams:
                team_general = await bot.fetch_channel(rt.discord_channel_team_general_id)
                await team_general.send(
                    content=f'`Functionality might be unavailable temporarily for around one hour after an update due to'
                            f' discord caching`',
                    embed=embed)
