import functools
import sys
from collections.abc import Callable
from typing import Union, Optional
from uuid import uuid4

from discord_slash import SlashContext, ComponentContext, MenuContext
from discord_slash.error import SlashCommandError

from blululu.logger import logger


class SlashCommandException(SlashCommandError):
    def __init__(self, message: str, send_new: Optional[bool] = None, hidden: Optional[bool] = None,
                 remove_embed: bool = False, remove_components: bool = False) -> None:
        self.message = message
        self.send_new = send_new
        self.hidden = hidden
        self.remove_embed = remove_embed
        self.remove_components = remove_components
        super().__init__(message)


async def send_error_message(ctx: Union[SlashContext, ComponentContext, MenuContext],
                             error_message: str,
                             send_new: Optional[bool] = None,
                             hidden: Optional[bool] = None,
                             remove_embed: bool = False,
                             remove_components: bool = False) -> None:
    if send_new is not None:
        _send_new = send_new
    elif isinstance(ctx, (MenuContext, ComponentContext)) and ctx._deferred_edit_origin:
        _send_new = True
        _hidden = True
    else:
        _send_new = isinstance(ctx, (SlashContext, MenuContext))

    if hidden is not None:
        _hidden = hidden
    elif ctx.deferred and ctx._deferred_hidden:
        _hidden = True
    elif isinstance(ctx, (MenuContext, ComponentContext)) and ctx._deferred_edit_origin:
        _hidden = True
    else:
        _hidden = False

    kwargs = dict(content=error_message, hidden=_hidden)
    if remove_embed:
        kwargs['embed'] = None

    if remove_components:
        kwargs['components'] = None

    if _send_new:
        await ctx.send(**kwargs)
    else:
        await ctx.edit_origin(**kwargs)


def slash_error_handler(func: Callable):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        for c in list(args) + list(kwargs.values()):
            if isinstance(c, (SlashContext, ComponentContext, MenuContext)):
                ctx = c
                break
        else:
            raise TypeError("Could not find the context in the function, make sure the function contain a discord "
                            "context")

        try:
            await func(*args, **kwargs)
        except SlashCommandException as e:
            exc_info = sys.exc_info()
            ERROR_ID = uuid4()
            logger.error(f"got an expected slash error `[ID={ERROR_ID}]`", exc_info=exc_info)
            await send_error_message(ctx,
                                     error_message=f"ERROR: {e.message}\n`[ID={ERROR_ID}]`",
                                     send_new=e.send_new,
                                     hidden=e.hidden,
                                     remove_embed=e.remove_embed,
                                     remove_components=e.remove_components)
        except Exception:
            ERROR_ID = uuid4()
            exc_info = sys.exc_info()
            logger.error(f"got an unexpected slash error `[ID={ERROR_ID}]`", exc_info=exc_info)
            await send_error_message(ctx, error_message=f"ERROR 500: if the error is critical please report it to the "
                                                        f"admin via the contact info present in `/about` with the error"
                                                        f"id \n`[ID={ERROR_ID}]`")
            # raise

    return wrapper
