import discord
from discord_slash import SlashContext, SlashCommand

from blululu.commands._utils import MyCoggy, HelpMessage, HelpMessageCommandExample
from blululu.config import admins, version


class About(MyCoggy):
    about_help_message = HelpMessage(
        description='To check more information about the bot',
        examples=[
            HelpMessageCommandExample(
                goal='I would like to know who is this amazing person that created this bot',
                command='/about'
            ),
            HelpMessageCommandExample(
                goal='I would like to see who is the admin for this bot',
                command='/about'
            ),
            HelpMessageCommandExample(
                goal='I would like to see the source code for this bot to make sure it is not spyware',
                command='/about'
            )
        ]
    )

    @MyCoggy.slash(name='about',
                   description="about the bot",
                   help_message=about_help_message)
    async def about(self, ctx: SlashContext):
        content = f"""
This bot is written by:
- Laser Racer (cat@heaven.ist , author@blulu.lu)
- Dahazza

administered by:
"""
        for admin_name, admin_info in admins.items():
            content += f'- {admin_name} <@{admin_info["discord_id"]}> ({admin_info["email"]})\n'

        content += f"""\nThe project is licensed under GPLv3, for the source code you can check it here
https://git.blulu.lu

For technical issues contact the admins or email (issues@blulu.lu).
For help about bot usage, check `/help` command first then contact the admins.
`version: {version}`
"""
        await ctx.send(content=content)


def setup(slash: SlashCommand, bot: discord.Client):
    About().setup(slash=slash, bot=bot)
