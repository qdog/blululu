from __future__ import annotations

import base64
import gzip
import json
import math
import typing
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from enum import Enum
from pprint import pprint
from typing import Optional, Union

from blululu.logger import logger

import aiohttp
import discord
import sqlalchemy.exc
import sqlalchemy.ext.asyncio
from discord import Role
from discord_slash import SlashCommand, SlashContext, SlashCommandOptionType, ButtonStyle, ComponentContext, \
    ContextMenuType, MenuContext
from discord_slash.utils.manage_commands import create_option
from discord_slash.utils.manage_components import create_actionrow, create_button, create_select, create_select_option
from sqlalchemy import true
from sqlalchemy.sql.elements import BinaryExpression, BooleanClauseList

from blululu.commands._utils import CacheManager, MatchProposalMessageHandler, ComponentsIDs, MyCoggy, HelpMessage, \
    HelpMessageCommandExample, HelpMessageMenuExample
from blululu.commands.configure import convert_vrml_api_team_info_to_row, get_vrml_team_by_id, \
    get_vrml_team_by_name
from blululu.commands.updatetimeslots import TimeSelector
from blululu.erros import SlashCommandException
from blululu.tables import Team, RegisteredTeam, MatchProposal, CanYouPlay


# @overload
# async def get_days_times_mapping_from_db(role_id: int, min_players: int, proposerer_id: int,
#                                          normalize: Literal[True] = True) -> tuple[
#     dict[datetime, dict[timedelta, bool]],
#     dict[datetime, dict[timedelta, int]]]:
#     ...
#
#
# @overload
# async def get_days_times_mapping_from_db(role_id: int, min_players: int, proposerer_id: int,
#                                          normalize: Literal[False] = False) -> tuple[list[datetime], list[int]]:
#     ...


async def get_days_times_mapping_from_db(role_id: int, min_players: int, proposerer_id: int, normalize: bool = True,
                                         n_consecutive_hours: float = 1, include_maybe: bool = True,
                                         other_conditions: Optional[Union[BinaryExpression, BooleanClauseList]] = None) \
        -> tuple[dict[datetime, bool], dict[datetime, int]]:
    # -> Union[tuple[dict[datetime, dict[timedelta, bool]],
    #                dict[datetime, dict[timedelta, int]]],
    #          tuple[list[datetime], list[int]]]:
    from blululu.tables import async_engine, TimeSlot, User, UserRegisteredTeamLink, \
        RegisteredTeam
    from sqlmodel.ext.asyncio.session import AsyncSession
    from sqlmodel import select

    other_conditions_processed = true() if other_conditions is None else other_conditions
    async with AsyncSession(async_engine) as session:
        # this is kind of natural join since we already defined the foreign keys
        # https://docs.sqlalchemy.org/en/14/orm/queryguide.html#joins-to-a-target-entity-or-selectable
        # TODO: find a way to make the bellow thingy more SQLy

        # statement = select(TimeSlot, func.count()).join(User
        #                                                 ).join(UserRegisteredTeamLink
        #                                                        ).join(RegisteredTeam
        #                                                               ).where(
        #     RegisteredTeam.discord_role_id == role_id,
        #     (TimeSlot.can_play == CanYouPlay.YES) | (TimeSlot.can_play == CanYouPlay.MAYBE),
        #     TimeSlot.match_id == None,  # noqa
        # ).group_by(TimeSlot.date).having(func.count() >= min_players)
        # result: list[tuple[TimeSlot, int]] = (await session.exec(statement)).all()
        # dates = [(i.date, c) for i, c in result]

        can_play_condition = (TimeSlot.can_play == CanYouPlay.YES)

        if include_maybe:
            can_play_condition |= TimeSlot.can_play == CanYouPlay.MAYBE

        current_time = datetime.now().astimezone(timezone.utc)

        result: list[TimeSlot] = (await session.exec(
            select(TimeSlot).join(User  # type: ignore
                                  ).join(UserRegisteredTeamLink
                                         ).join(RegisteredTeam
                                                ).where(
                RegisteredTeam.discord_role_id == role_id,
                can_play_condition,
                other_conditions_processed,
                TimeSlot.match_id == None,  # noqa
                TimeSlot.date >= current_time,
                (current_time + timedelta(weeks=1)) >= TimeSlot.date,
            )
        )).all()
        groups_by_user: dict[int, list[TimeSlot]] = dict()

        match_proposals: list[MatchProposal] = []
        for ts in result:
            match_proposals.extend(await session.run_sync(lambda _session: ts.match_proposals))
        logger.debug(f"{match_proposals=}")
        for t in result:
            if t.user_id not in groups_by_user:
                groups_by_user[t.user_id] = []
            groups_by_user[t.user_id].append(t)

        n_consecutive_dates = int(2 * n_consecutive_hours)
        filtered_timeslots: list[TimeSlot] = []
        for group in groups_by_user.values():
            group_sorted = sorted(group, key=lambda x: x.date)
            for i in range(len(group_sorted) - 1):
                continues_free = True
                for j in range(i, min(len(group_sorted) - 1, n_consecutive_dates + i)):
                    if (group_sorted[j + 1].date - group_sorted[j].date) != timedelta(minutes=30):
                        continues_free = False
                        break

                if continues_free:
                    filtered_timeslots.append(group_sorted[i])

        groups_day: dict[datetime, list[TimeSlot]] = dict()
        for t in filtered_timeslots:
            if t.date not in groups_day:
                groups_day[t.date] = []
            groups_day[t.date].append(t)

        dates_to_delete = []
        for date, list_of_timeslots in groups_day.items():
            if len(list_of_timeslots) < min_players:
                dates_to_delete.append(date)

        for date in dates_to_delete:
            groups_day.pop(date)

        dates = [(d, len(p)) for d, p in groups_day.items()]
        dates.sort()
        if not normalize:
            return {i: False for i, _ in dates}, {i: c for i, c in dates}

        try:
            user: User = (await session.exec(
                select(User).where(User.id == proposerer_id))  # type: ignore
                          ).one()
        except sqlalchemy.exc.NoResultFound:
            raise SlashCommandException(message="Could not find user configuration. Try `/config user` first.")
        user_timezone = user.timezone
        correct_timezone = [(i.astimezone(tz=timezone(offset=timedelta(hours=user_timezone))), c) for i, c in dates]
        logger.debug(f"{user=}, {result=}")

    # days_times_mapping = dict()
    # days_times_count_mapping = dict()
    # for dt, c in correct_timezone:
    #     day = datetime(year=dt.year, month=dt.month, day=dt.day, tzinfo=dt.tzinfo)
    #     logger.debug(f"{day=}")
    #     logger.debug(f"{day in days_times_mapping=}")
    #     logger.debug(f"{days_times_mapping.keys()=}")
    #     if day not in days_times_mapping:
    #         days_times_mapping[day] = dict()
    #         days_times_count_mapping[day] = dict()
    #     days_times_mapping[day][dt - day] = False
    #     days_times_count_mapping[day][dt - day] = c

    return {d: False for d, _ in correct_timezone}, {i: c for i, c in dates}


def is_positive_int(string: str) -> bool:
    try:
        if len(set(string) - set('0123456789')) > 0:
            raise SlashCommandException(message='listen, i know you really want to do something naughty, '
                                                'please respect your self and stop doing this.')
        x = int(string)
        return x >= 0
    except TypeError:
        return False


def clean_condition(condition: str) -> list[str]:
    condition_list = condition.strip().split()
    cond = [i.lower() for i in condition_list if len(i) != 0]
    return cond


class ConditionOperators(Enum):
    AND = 'and'
    OR = 'or'


class MixOperationException(SlashCommandException):
    def __init__(self, current_operator: ConditionOperators, other_operator: ConditionOperators) -> None:
        message = 'for easier processing and readability, we dont allow a mix of operations. add brackets between ' \
                  'different operations to signify which is first. ' \
                  f'For example, `(@larry {current_operator.value} @the) {other_operator.value} @cow` would be ' \
                  f'valid instead of `@larry {current_operator.value} @the {other_operator.value} @cow`'
        super().__init__(message)


class Condition:
    from blululu.tables import User
    from sqlmodel import and_, or_
    OPENING_BRACKET = '('
    CLOSING_BRACKET = ')'
    operator_mapping = {
        ConditionOperators.AND: and_,
        ConditionOperators.OR: or_
    }

    def __init__(self, condition: list[str]):
        self.my_conditions: list[Union[Condition, BinaryExpression]] = []
        self.operator_type: Optional[ConditionOperators] = None
        self.parse(condition)

    def parse(self, condition: list[str]):
        level: int = 0
        start_other_cond: Optional[int] = None
        for index, condy in enumerate(condition):
            if condy == self.OPENING_BRACKET:
                if level == 0:
                    start_other_cond = index
                level += 1
            elif condy == self.CLOSING_BRACKET:
                level -= 1

            if level == 0 and start_other_cond is not None:
                self.my_conditions.append(Condition(condition[start_other_cond + 1: index]))
                start_other_cond = None

            elif level == 0:
                if condy == 'and':
                    current_operator = ConditionOperators.AND
                    if self.operator_type is not None and self.operator_type != current_operator:
                        raise MixOperationException(current_operator=current_operator,
                                                    other_operator=self.operator_type)
                    self.operator_type = current_operator
                elif condy == 'or':
                    current_operator = ConditionOperators.OR
                    if self.operator_type is not None and self.operator_type != current_operator:
                        raise MixOperationException(current_operator=current_operator,
                                                    other_operator=self.operator_type)
                    self.operator_type = current_operator
                elif condy[:3] == '<@!' and condy[-1] == '>' and is_positive_int(condy[3:-1]):
                    user_cond: BinaryExpression = self.User.id == int(condy[3:-1])  # type: ignore
                    self.my_conditions.append(user_cond)
                else:
                    if condy[:1] == '<':
                        raise SlashCommandException(
                            message=f'Could not process {condy} part, make sure it is a valid user. '
                                    f'dont think you are smart by using roles here. '
                                    f'dont play these games')
                    else:
                        raise SlashCommandException(
                            message=f'Could not process `{condy}` part, make sure the condition is '
                                    f'valid')

    def process(self) -> Union[BooleanClauseList, BinaryExpression]:
        processed_conditions: list[Union[BooleanClauseList, BinaryExpression]] = []
        for p in self.my_conditions:
            if isinstance(p, Condition):
                processed_conditions.append(p.process())
            else:
                processed_conditions.append(p)
        if len(processed_conditions) > 1:
            if self.operator_type is None:
                raise ValueError("Operator type could not be found")
            return self.operator_mapping[self.operator_type](*processed_conditions)
        elif len(processed_conditions) == 1:
            return processed_conditions[0]
        else:
            raise ValueError("we should not reach this line")

    def __repr__(self) -> str:
        return f"Condition(operator={self.operator_type}, sequence={repr(self.my_conditions)})"


def process_condition(condition: str) -> Union[BooleanClauseList, BinaryExpression]:
    clean = clean_condition(condition)
    cond = Condition(clean)
    return cond.process()


@dataclass
class ProposeCacheInfo:
    my_team: Team
    enemy_team: Team
    my_registered_team: RegisteredTeam
    selected_times: list[datetime]
    match_duration: float
    match_comment: Optional[str]


class Propose(MyCoggy):
    component_id = ComponentsIDs()

    def __init__(self):
        self.time_selector = self.attach_coggy(TimeSelector(allow_multi_choice=False,
                                                            get_components_callback=self.propose_components))
        self.match_proposal_message_handler = self.attach_coggy(MatchProposalMessageHandler())
        self.cache: CacheManager[tuple[int, int], ProposeCacheInfo] = CacheManager()

    propose_help_message = HelpMessage(
        description="to propose a match to the team.",
        examples=[
            HelpMessageCommandExample(
                goal='I want to propose a match where there are at least 2 available players',
                command='/propose manual team:@MyTeam against:ignite min_players:2'
            ),
            HelpMessageCommandExample(
                goal='I want to propose a match where the probability of players saying yes is higher and i dont mind '
                     'seeing lower available timeslots',
                command='/propose manual team:@MyTeam against:ignite mode:confident'
            ),
            HelpMessageCommandExample(
                goal='I want to propose a match with more timeslots even if it means lower probability of players '
                     'saying yes',
                command='/propose manual team:@MyTeam against:ignite mode:complete'
            ),
            HelpMessageCommandExample(
                goal='I want to find timeslots where (either of our two goalies available) and our main striker is '
                     'available',
                command='/propose manual team:@MyTeam against:ignite '
                        'condition:(@OurGoalie1 OR @OurGoalie2) AND @MainStriker'
            )
        ]
    )

    @MyCoggy.subcommand(
        base='propose',
        name="manual",
        description="propose a match manually",
        options=[
            create_option(name="team", option_type=SlashCommandOptionType.ROLE, required=True,
                          description="the team role"),
            create_option(name="against", option_type=SlashCommandOptionType.STRING, required=True,
                          description="The name of the enemy team"),
            create_option(name="duration", option_type=SlashCommandOptionType.FLOAT, required=False,
                          description="the match length (default: 1 hour)",
                          choices=[dict(name=f"{int(i / 2)}h {int(i * 30) % 60}m", value=i / 2) for i in
                                   range(1, 3 * 2 + 1)]),
            create_option(name="min_players", option_type=SlashCommandOptionType.INTEGER, required=False,
                          description="The minimum number of players needed for the match (default: 4)",
                          choices=list(range(1, 10 + 1))),
            create_option(name="mode",
                          description="choose the mode for what to prioritize when prefilling the timeslots "
                                      "menus. (default: complete)",
                          choices=[dict(name="complete", value=1), dict(name="confident", value=0)],
                          required=False,
                          option_type=SlashCommandOptionType.INTEGER
                          ),
            create_option(name="condition",
                          description="choose condition you want to apply for the search. (/help propose might help)",
                          required=False,
                          option_type=SlashCommandOptionType.STRING
                          ),
            create_option(name="comment",
                          description="the match comment, 'this is a scrim', 'training for VRML' etc.",
                          option_type=SlashCommandOptionType.STRING,
                          required=False)
        ],
        base_default_permission=False,
        command_group='after_config_user',
        help_message=propose_help_message,
    )
    async def propose_manual(
            self,
            ctx: SlashContext,
            team: Role,
            against: str,
            duration: float = 1,
            min_players: int = 4,
            mode: int = 1,
            condition: Optional[str] = None,
            comment: Optional[str] = None
    ):
        await self.propose(
            ctx=ctx,
            team=team,
            against=against,
            duration=duration,
            min_players=min_players,
            mode=mode,
            condition=condition,
            comment=comment
        )

    propose_help_message.add_examples(examples=[
        HelpMessageCommandExample(goal='I would like to quickly propose a match from the encoded timeslots i got from '
                                       'another team which has the bot. (check `/help encode-timeslots` for more info)',
                                  command='/propose automatic team:@MyTeam timeslots:ABzY8lM(D;0{....'),
        HelpMessageCommandExample(goal='I have the encoded timeslots and I would like to choose manually which of the '
                                       'shared timeslots i want to propose a match for. '
                                       '(check `/help encode-timeslots` for more info)',
                                  command='/propose automatic team:@MyTeam timeslots:ABzY8lM(D;0{.... menu:True')
    ])

    @MyCoggy.subcommand(
        base='propose',
        name="automatic",
        description="propose with the aid of encoded timeslots",
        options=[
            create_option(name="team", option_type=SlashCommandOptionType.ROLE, required=True,
                          description="the team role"),
            create_option(name="timeslots", option_type=SlashCommandOptionType.STRING, required=True,
                          description="The encoded timeslots."),
            create_option(name="max_proposals", option_type=SlashCommandOptionType.INTEGER, required=False,
                          choices=[dict(name=str(i), value=i) for i in range(1, 10 + 1)[::-1]],
                          description="Maximum number of auto generated proposals. (default: 5)"),
            create_option(name="menu", option_type=SlashCommandOptionType.BOOLEAN, required=False,
                          description="Show selection menu. (default: false)"),
            create_option(name="duration", option_type=SlashCommandOptionType.FLOAT, required=False,
                          description="the match length (default: 1 hour)",
                          choices=[dict(name=f"{int(i / 2)}h {int(i * 30) % 60}m", value=i / 2) for i in
                                   range(1, 3 * 2 + 1)]),
            create_option(name="min_players", option_type=SlashCommandOptionType.INTEGER, required=False,
                          description="The minimum number of players needed for the match (default: 4)",
                          choices=list(range(1, 10 + 1))),
            create_option(name="mode",
                          description="choose the mode for what to prioritize when prefilling the timeslots "
                                      "menus. (default: complete)",
                          choices=[dict(name="complete", value=1), dict(name="confident", value=0)],
                          required=False,
                          option_type=SlashCommandOptionType.INTEGER
                          ),
            create_option(name="condition",
                          description="choose condition you want to apply for the search. (/help propose might help)",
                          required=False,
                          option_type=SlashCommandOptionType.STRING
                          ),
            create_option(name="comment",
                          description="the match comment, 'this is a scrim', 'training for VRML' etc.",
                          option_type=SlashCommandOptionType.STRING,
                          required=False)
        ],
        base_default_permission=False,
        command_group='after_config_user',
        help_message=propose_help_message,
    )
    async def propose_automatic(
            self,
            ctx: SlashContext,
            team: Role,
            timeslots: str,
            max_proposals: Optional[int] = 5,
            menu: bool = False,
            duration: Optional[float] = None,
            min_players: Optional[int] = None,
            mode: int = 1,
            condition: Optional[str] = None,
            comment: Optional[str] = None
    ):
        try:
            extra_info, table = await ViewTimeSlots.decode_timeslots(encoded_timeslots=timeslots)
        except Exception:
            raise MessageIsNotTimeSlot()

        await self.propose(
            ctx=ctx,
            team=team,
            duration=duration,
            min_players=min_players,
            mode=mode,
            condition=condition,
            comment=comment,
            timeslots_extra_info=extra_info,
            timeslots_table=table,
            max_proposals=max_proposals,
            show_menu=menu,
        )

    def normalize_timeslots(self, timeslots: dict[datetime, int]) -> dict[datetime, float]:
        max_v_timeslots = max(timeslots.values())
        min_v_timeslots = max(timeslots.values())
        min_max = (max_v_timeslots - min_v_timeslots)
        new_timeslots = dict()
        for t, v in timeslots.items():
            # v = (v-min_v_timeslots) / min_max
            new_timeslots[t] = v / max_v_timeslots

        return new_timeslots

    def get_shared_timeslots(self, my_timeslots: dict[datetime, int], enemy_timeslots: dict[datetime, int]) \
            -> list[datetime]:
        my_timeslots_normalized = self.normalize_timeslots(my_timeslots)
        enemy_timeslots_normalized = self.normalize_timeslots(enemy_timeslots)
        my_timeslots_normalized_set = set(my_timeslots_normalized.keys())
        enemy_timeslots_normalized_set = set(enemy_timeslots_normalized.keys())
        shared_timeslots = list(my_timeslots_normalized_set.intersection(enemy_timeslots_normalized_set))
        importance: dict[datetime, float] = dict()
        for t in shared_timeslots:
            my_importance = my_timeslots_normalized[t] ** 0.5
            enemy_importance = enemy_timeslots_normalized[t] ** 0.5
            importance[t] = my_importance + enemy_importance

        shared_timeslots.sort(key=lambda t: importance[t], reverse=True)

        return shared_timeslots

    async def propose(
            self,
            ctx: SlashContext,
            team: Role,
            against: Optional[str] = None,
            against_id: Optional[str] = None,
            duration: Optional[float] = None,
            min_players: Optional[int] = None,
            mode: int = 1,
            condition: Optional[str] = None,
            comment: Optional[str] = None,
            timeslots_extra_info: Optional[dict[ViewTimeSlots.ExtraInfo, Union[str, int, float]]] = None,
            timeslots_table: Optional[dict[ViewTimeSlots.Columns, list[Union[str, int, tuple[int, int]]]]] = None,
            max_proposals: Optional[int] = None,
            show_menu: bool = True
    ):
        await ctx.defer(hidden=True)
        shared_times = None
        if condition is not None and len(condition) > 0:
            other_conditions = process_condition(condition=condition)
        else:
            other_conditions = None

        if timeslots_extra_info:
            if against_id is None:
                against_id_ = timeslots_extra_info[ViewTimeSlots.ExtraInfo.team_id]
                if against_id_ is not None:
                    against_id = str(against_id_)

            if min_players is None:
                min_players = int(timeslots_extra_info[ViewTimeSlots.ExtraInfo.min_players])
            if duration is None:
                duration = float(timeslots_extra_info[ViewTimeSlots.ExtraInfo.duration])

        if min_players is None:
            raise ValueError("please specify the minimum number of players")
        if duration is None:
            raise ValueError("please specify the match duration")

        from blululu.tables import async_engine, RegisteredTeam
        from sqlmodel.ext.asyncio.session import AsyncSession
        from sqlmodel import select

        async with AsyncSession(async_engine) as session:
            try:
                registered_team: RegisteredTeam = (await session.exec(
                    select(RegisteredTeam).where(RegisteredTeam.discord_role_id == team.id)  # type: ignore
                )).one()
            except sqlalchemy.exc.NoResultFound:
                raise SlashCommandException(message=f'The {team.mention} role is not a registered team role. '
                                                    f'Make sure the role is representing a team')

        include_maybe = bool(mode)
        role = team
        logger.debug(f"{against_id=}, {min_players=}, {duration=}")
        days_times_mapping, dt_mapping_pcount = await get_days_times_mapping_from_db(role_id=role.id,
                                                                                     min_players=min_players,
                                                                                     proposerer_id=ctx.author_id,
                                                                                     n_consecutive_hours=duration,
                                                                                     include_maybe=include_maybe,
                                                                                     other_conditions=other_conditions)

        if timeslots_table is not None:
            times_raw = timeslots_table[ViewTimeSlots.Columns.time].copy()
            times: list[datetime] = []
            for tr in times_raw:
                assert not isinstance(tr, tuple)
                times.append(datetime.fromtimestamp(int(tr), timezone.utc))

            importance: dict[datetime, int]
            if ViewTimeSlots.Columns.players in timeslots_table:
                importance = dict((t, v) for t, v in  # type: ignore
                                  zip(times,
                                      timeslots_table[ViewTimeSlots.Columns.players])
                                  )
            else:
                importance = dict((t, (2 - v) + 1) for t, v in  # type: ignore
                                  zip(times,
                                      timeslots_table[ViewTimeSlots.Columns.availability])
                                  )

            shared_times = self.get_shared_timeslots(my_timeslots=dt_mapping_pcount, enemy_timeslots=importance)

            shared_times_set = set(shared_times)

            for t in list(days_times_mapping.keys()):
                if t not in shared_times_set:
                    days_times_mapping.pop(t)
                    dt_mapping_pcount.pop(t)

        if len(days_times_mapping) == 0:
            raise SlashCommandException(message="No shared available time slots were found. Ask the team to do "
                                                "`/update-timeslots` or reduce the number of minimum required players")
        from blululu.tables import async_engine, RegisteredTeam, Team
        from sqlmodel.ext.asyncio.session import AsyncSession
        from sqlmodel import select
        async with AsyncSession(async_engine) as session:
            my_team: Team = (await session.exec(
                select(Team).join(RegisteredTeam).where(RegisteredTeam.discord_role_id == role.id)  # type: ignore
            )).one()
            my_registered_team_db: list[RegisteredTeam] = await session.run_sync(
                # im not sure what is wrong with sqlmodel
                lambda _session: my_team.registered_team)  # type: ignore

        my_registered_team: Optional[RegisteredTeam]

        if len(my_registered_team_db) == 1:
            my_registered_team = my_registered_team_db[0]
        elif len(my_registered_team_db) == 0:
            raise SlashCommandException(message="the team is not registered, make sure you run `/config-team` first")
        else:
            raise ValueError("did not expect to get multiple registered teams for one team")

        async with aiohttp.ClientSession() as http_session:
            if against_id is None:
                if against is None:
                    raise ValueError("please specify the team you want to play against")
                info = await get_vrml_team_by_name(game=my_team.game, name=against, session=http_session)
                against_id = info[0]["id"]
            enemy_team_info = await get_vrml_team_by_id(team_id=against_id, session=http_session)

        enemy_team = convert_vrml_api_team_info_to_row(info=enemy_team_info, game=my_team.game)
        logger.debug(f"{repr(my_registered_team)=}")
        if show_menu:
            self.cache[ctx.author_id, ctx.channel_id] = ProposeCacheInfo(
                my_team=my_team, enemy_team=enemy_team,
                my_registered_team=my_registered_team,
                selected_times=[],
                match_duration=duration,
                match_comment=comment)

            await self.time_selector.set_days_times_mapping(days_times_mapping, ctx.author_id)
            await ctx.send(content="proposals = []",
                           components=await self.time_selector.get_all_components(ctx.author_id),
                           hidden=True)
        else:
            assert shared_times is not None
            await self.match_proposal_message_handler.create_threaded_proposals(
                ctx=ctx,
                my_team=my_team,
                my_registered_team=my_registered_team,
                enemy_team=enemy_team,
                selected_times=shared_times[:max_proposals],
                match_duration=duration,
                match_comment=comment
            )

            await ctx.send(content="match proposed", hidden=True)

    async def propose_components(self, author_id: int) -> list[dict]:
        add_and_remove_buttons = [
            create_button(
                style=ButtonStyle.green,
                label="Add",
                custom_id=self.component_id.add_button(self)
            ),
            create_button(
                style=ButtonStyle.red,
                label="Pop",
                custom_id=self.component_id.pop_button(self)
            ),
        ]
        buttons = [
            create_button(
                style=ButtonStyle.blue,
                label="Propose",
                custom_id=self.component_id.propose_button(self)
            ),
        ]
        return [create_actionrow(*add_and_remove_buttons), create_actionrow(*buttons)]

    async def update_current_proposals_message(self, ctx: ComponentContext):
        utc_timestamps = [int(t.astimezone(timezone.utc).timestamp())
                          for t in self.cache[ctx.author_id, ctx.channel_id].selected_times]
        times = [f"<t:{t}:F>" for t in utc_timestamps]
        times_one_line = '[' + ', '.join(times) + ']'
        await ctx.edit_origin(content=f"proposals = {times_one_line}")

    @MyCoggy.component_callback(components=component_id.add_button)
    async def add_callback(self, ctx: ComponentContext):
        selected_time = await self.time_selector.get_selected_days_and_times(ctx.author_id)
        if selected_time is None:
            raise SlashCommandException(message="please select a time before proposing. idiot.")
        if selected_time not in self.cache[ctx.author_id, ctx.channel_id].selected_times:
            self.cache[ctx.author_id, ctx.channel_id].selected_times.append(selected_time)
            await self.update_current_proposals_message(ctx=ctx)
        else:
            await ctx.defer(ignore=True)

    @MyCoggy.component_callback(components=component_id.pop_button)
    async def pop_callback(self, ctx: ComponentContext):
        if len(self.cache[ctx.author_id, ctx.channel_id].selected_times) > 0:
            self.cache[ctx.author_id, ctx.channel_id].selected_times.pop(-1)
            await self.update_current_proposals_message(ctx=ctx)
        else:
            await ctx.defer(ignore=True)

    @MyCoggy.component_callback(components=component_id.propose_button)
    async def propose_callback(self, ctx: ComponentContext):

        cache_info = self.cache[ctx.author_id, ctx.channel_id]
        if len(cache_info.selected_times) == 0:
            raise SlashCommandException(
                message="please select a time before proposing and press 'add'. idiot.",
                send_new=True,
                hidden=True,
            )

        await ctx.defer(edit_origin=True)
        await ctx.edit_origin(content="Thank you, now the match is being proposed", components=[])

        await self.match_proposal_message_handler.create_threaded_proposals(
            ctx=ctx,
            my_team=cache_info.my_team,
            my_registered_team=cache_info.my_registered_team,
            enemy_team=cache_info.enemy_team,
            selected_times=cache_info.selected_times,
            match_duration=cache_info.match_duration,
            match_comment=cache_info.match_comment)

        await ctx.send(content="match proposed", hidden=True)


class MessageIsNotTimeSlot(SlashCommandException):
    def __init__(self) -> None:
        message = "The message is not in the correct timeslot format to be decoded."
        super().__init__(message=message, hidden=True)


@dataclass
class VTSCacheInfo:
    days_times_mapping: dict[datetime, bool]
    dt_mapping_pcount: dict[datetime, int]
    copy_mode: bool
    show_n_aval_players: bool
    team_id: str
    min_players: int
    duration: float
    total_pages: int
    current_page: int


class ViewTimeSlots(MyCoggy):
    component_id = ComponentsIDs()

    PAGE_SIZE = 42

    def __init__(self):
        self.color_symbols = ["🟢", "🟡", "🟠"]
        self.cache: CacheManager[tuple[int, int], VTSCacheInfo] = CacheManager()

    async def view_timeslots_components(self, cache_info: VTSCacheInfo) -> list[dict]:
        action_rows = []
        buttons = [
            create_button(
                style=ButtonStyle.red if cache_info.copy_mode else ButtonStyle.green,
                label="toggle copy mode",
                custom_id=self.component_id.viewall_copy_button(self)
            ),
            create_button(
                style=ButtonStyle.red if cache_info.show_n_aval_players else ButtonStyle.green,
                label="toggle number of players",
                custom_id=self.component_id.number_of_players_button(self)
            ),
        ]
        action_rows.append(create_actionrow(*buttons))

        if cache_info.total_pages > 1:
            pages_section = [
                create_select(
                    custom_id=self.component_id.view_timeslot_pages_select(self),
                    options=[
                        create_select_option(label=f"Page {i}", value=str(i), default=(i == cache_info.current_page))
                        for i in range(cache_info.total_pages)
                    ],
                    placeholder="Choose the page",
                    min_values=1,
                    max_values=1
                )
            ]
            action_rows.append(create_actionrow(*pages_section))
        return action_rows

    class Columns(Enum):
        index = 'Slot-Number'
        players = 'players'
        availability = 'availability'
        time = 'time'

    class ExtraInfo(Enum):
        team_id = 'Team ID'
        duration = 'Duration'
        min_players = 'Number of Players'

    @typing.no_type_check
    async def create_view_message(self, cache_info: VTSCacheInfo) -> str:
        dt_mapping_pcount = cache_info.dt_mapping_pcount
        days_times_mapping = cache_info.days_times_mapping
        copy_mode = cache_info.copy_mode
        show_n_aval_players = cache_info.show_n_aval_players
        current_page = cache_info.current_page

        timeslots_ordered = sorted(days_times_mapping.keys())
        symbols: dict[datetime, str] = dict()
        max_c = max(dt_mapping_pcount.values())
        color_symbol_array = self.color_symbols
        for d in timeslots_ordered:
            diff = min(max_c - dt_mapping_pcount[d], len(color_symbol_array) - 1)
            symbols[d] = color_symbol_array[diff]

        columns: dict[ViewTimeSlots.Columns, list[str]] = dict((c, []) for c in self.Columns)
        needed_padding: dict[ViewTimeSlots.Columns, float] = dict((i, len(i.value) / 2) for i in columns)
        n_rows = 0
        for i, t in enumerate(timeslots_ordered):
            if (current_page * self.PAGE_SIZE) > i:
                continue
            elif i > ((current_page + 1) * self.PAGE_SIZE):
                break
            else:
                columns[self.Columns.index].append(f'-[{i}]')
                columns[self.Columns.players].append(f'({dt_mapping_pcount[t]})')
                columns[self.Columns.availability].append(f'{symbols[t]}')
                columns[self.Columns.time].append(f'`<t:{int(t.timestamp())}:F>')
                n_rows += 1

        current_used_columns = []
        for column in self.Columns:
            if column is self.Columns.players and not show_n_aval_players:
                continue
            current_used_columns.append(column)

        extra_info: dict[ViewTimeSlots.ExtraInfo, str] = dict()
        extra_info[self.ExtraInfo.team_id] = cache_info.team_id
        extra_info[self.ExtraInfo.duration] = f"{cache_info.duration}h"
        extra_info[self.ExtraInfo.min_players] = str(cache_info.min_players)

        extra_info_lines = [f'`{h.value}: {v}`' for h, v in extra_info.items()]

        table_lines = ['`' + ' '.join(f'{c.value}' for c in current_used_columns) + '`']
        for row_id in range(n_rows):
            row_str = []
            for column in current_used_columns:
                content = columns[column][row_id]
                padding, reminder = divmod(needed_padding[column] - len(content) // 2, 1)
                padding = int(padding)
                reminder = int(reminder)
                first_padding = padding
                second_padding = padding
                if reminder > 0:
                    first_padding += 1

                if len(content) % 2 == 1:
                    second_padding -= 1
                if column == current_used_columns[0]:
                    row_str.append('`')
                row_str.append(' ' * first_padding)
                row_str.append(content)

                row_str.append(' ' * second_padding)

            table_lines.append(''.join(row_str))

        separator = f'\n`{"-" * max(map(len, extra_info_lines))}`\n'
        content = '\n'.join(extra_info_lines) + separator + "\n".join(table_lines)

        if copy_mode:
            content = f"```{content}```"

        return content

    propose_help_message = HelpMessage(
        description="to propose a match to the team.",
        examples=[
            HelpMessageMenuExample(
                use_case='I would like to use the output of `/view-timeslots` of another team to create match proposal'
                         ' for my team based on the shared timeslots. '
                         '(check the output of `/help propose` in the automatic section)',
                img_link="https://i.imgur.com/USiaaS2.png"
            )
        ]
    )

    @typing.no_type_check
    @MyCoggy.context_menu(target=ContextMenuType.MESSAGE,
                          name="encode-timeslots",
                          help_message=propose_help_message,
                          )
    async def encode_timeslots(self, ctx: MenuContext):
        content = ctx.target_message.content.replace('`', '').split('\n')
        for index, line in enumerate(content):
            if set(line) == {'-'}:
                extra_info_lines = content[:index]
                table_lines = content[index + 1:]
                break
        else:
            raise MessageIsNotTimeSlot()

        extra_info_lines_clean: list[tuple[str, str]] = [i.strip().split(': ') for i in extra_info_lines]
        extra_info: dict[ViewTimeSlots.ExtraInfo, Union[str, float, int]] = dict(
            (self.ExtraInfo(key), value) for key, value in extra_info_lines_clean
        )
        extra_info[self.ExtraInfo.min_players] = int(extra_info[self.ExtraInfo.min_players])
        extra_info[self.ExtraInfo.duration] = float(extra_info[self.ExtraInfo.duration][:-1])  # type: ignore

        logger.debug(f"{extra_info=}")

        lines = [i.strip().split() for i in table_lines]
        headers: list[ViewTimeSlots.Columns] = [self.Columns(i) for i in lines[0]]
        rows = lines[1:]
        table: dict[ViewTimeSlots.Columns, list[Union[str, int, tuple[int, int]]]] = dict(
            (column, []) for column in headers)
        for row in rows:
            for value, header in zip(row, headers):
                table[header].append(value)

        del table[self.Columns.index]
        times = [int(i.replace('<t:', '').replace(':F>', '')) for i in table[self.Columns.time]]
        logger.debug(f"{times=}")
        times_compressed: list[Union[int, tuple[int, int]]] = [times[0]]
        for index in range(1, len(times)):
            if (times[index] - times[index - 1]) == 60 * 30:
                if isinstance(times_compressed[-1], int):
                    times_compressed[-1] = (times_compressed[-1], times[index])
                else:
                    times_compressed[-1] = (times_compressed[-1][0], times[index])
            else:
                times_compressed.append(times[index])

        table[self.Columns.time] = times_compressed
        table[self.Columns.availability] = [self.color_symbols.index(i) for i in table[self.Columns.availability]]
        if self.Columns.players in table:
            del table[self.Columns.availability]
            table[self.Columns.players] = [int(i.replace('(', '').replace(')', '')) for i in
                                           table[self.Columns.players]]

        logger.debug(f"{table=}")
        table = dict((key.name, value) for key, value in table.items())
        extra_info = dict((key.name, value) for key, value in extra_info.items())
        json_compression = json.dumps(dict(table=table, extra_info=extra_info)).encode()
        compressed = gzip.compress(json_compression)

        encode = base64.b64encode(compressed)
        logger.debug(f"{encode=}")
        logger.debug(f"{len(encode)=}")
        await ctx.send(
            content=f"```{encode.decode('utf-8')}```",
            hidden=True
        )

    @classmethod
    async def decode_timeslots(cls, encoded_timeslots: str) -> tuple[
        dict[ExtraInfo, Union[str, int, float]],
        dict[Columns, list[Union[str, int, tuple[int, int]]]]
    ]:
        decoded = base64.b64decode(encoded_timeslots)
        decompressed_gzip = gzip.decompress(decoded)
        data_raw = json.loads(decompressed_gzip)
        extra_info_raw = data_raw['extra_info']
        table_raw: dict[str, list[Union[str, int, tuple[int, int]]]] = data_raw['table']
        table: dict[ViewTimeSlots.Columns, list[Union[str, int, tuple[int, int]]]] = dict(
            (cls.Columns[key], value) for key, value in table_raw.items()
        )
        extra_info: dict[ViewTimeSlots.ExtraInfo, Union[str, int, float]] = dict(
            (cls.ExtraInfo[key], value) for key, value in extra_info_raw.items()
        )
        times: list[int] = []
        for t in table[cls.Columns.time]:
            if isinstance(t, int):
                times.append(t)
            elif isinstance(t, (list, tuple)):
                for i in range(t[0], t[1], 60 * 30):
                    times.append(i)
                times.append(t[1])
            else:
                raise ValueError("This line should not be reached")
        table[cls.Columns.time] = times  # type: ignore

        return extra_info, table

    view_timeslots_help_message = HelpMessage(
        description="""
        to propose a match to the team.
                """,
        examples=[
            HelpMessageCommandExample(
                goal='I want to propose a match where there are at least 2 available players',
                command='/view-timeslots min_players:2'
            ),
            HelpMessageCommandExample(
                goal='I want to propose a match where the probability of players saying yes is higher and i dont mind '
                     'seeing lower available timeslots',
                command='/view-timeslots team:@MyTeam mode:confident'
            ),
            HelpMessageCommandExample(
                goal='I want to propose a match with more timeslots even if it means lower probability of players '
                     'saying yes',
                command='/view-timeslots team:@MyTeam mode:complete'
            ),
            HelpMessageCommandExample(
                goal='I want to find timeslots where (either of our two goalies available) and our main striker is '
                     'available',
                command='/view-timeslots team:@MyTeam condition:(@OurGoalie1 OR @OurGoalie2) AND @MainStriker'
            )
        ]
    )

    @MyCoggy.slash(name="view-timeslots",
                   description="to view the time availability for the team",
                   options=[
                       create_option(name="team", option_type=SlashCommandOptionType.ROLE, required=True,
                                     description="the team role"),
                       create_option(name="duration", option_type=SlashCommandOptionType.FLOAT, required=False,
                                     description="the match length (default: 1 hour)",
                                     choices=[dict(name=f"{int(i / 2)}h {int(i * 30) % 60}m", value=i / 2) for i in
                                              range(1, 3 * 2 + 1)]),
                       create_option(name="min_players", option_type=SlashCommandOptionType.INTEGER, required=False,
                                     description="The name of the enemy team (default 4)",
                                     choices=list(range(1, 10 + 1))),
                       create_option(name="condition",
                                     description="choose condition you want to apply for the search.",
                                     required=False,
                                     option_type=SlashCommandOptionType.STRING
                                     ),
                       create_option(name="mode",
                                     description="choose the mode for what to prioritize when prefilling the timeslots "
                                                 "menus. (default: complete)",
                                     choices=[dict(name="complete", value=1), dict(name="confident", value=0)],
                                     required=False,
                                     option_type=SlashCommandOptionType.INTEGER
                                     )
                   ],
                   default_permission=False,
                   command_group='after_config_user',
                   help_message=view_timeslots_help_message
                   )
    async def view_timeslots(self, ctx: SlashContext, team: Role, duration: float = 1, min_players: int = 4,
                             condition: Optional[str] = None, mode: int = 1):
        logger.debug("GOT MESSAGE")
        await ctx.defer(hidden=True)
        if condition is not None and len(condition) > 0:
            other_conditions = process_condition(condition=condition)
        else:
            other_conditions = None

        from blululu.tables import async_engine, RegisteredTeam
        from sqlmodel.ext.asyncio.session import AsyncSession
        from sqlmodel import select
        async with AsyncSession(async_engine) as session:
            try:
                registered_team: RegisteredTeam = (await session.exec(
                    select(RegisteredTeam).where(RegisteredTeam.discord_role_id == team.id)  # type: ignore
                )).one()
            except sqlalchemy.exc.NoResultFound:
                raise SlashCommandException(message=f'The {team.mention} role is not a registered team role. '
                                                    f'Make sure the role is representing a team')

        include_maybe = bool(mode)
        days_times_mapping, dt_mapping_pcount = await get_days_times_mapping_from_db(role_id=team.id,
                                                                                     min_players=min_players,
                                                                                     proposerer_id=ctx.author_id,
                                                                                     normalize=False,
                                                                                     n_consecutive_hours=duration,
                                                                                     include_maybe=include_maybe,
                                                                                     other_conditions=other_conditions)
        if len(days_times_mapping) == 0:
            raise SlashCommandException(message="No shared available time slots were found. Ask the team to do "
                                                "`/update-timeslots` or reduce the number of minimum required players "
                                                "via the `min_players` option in the command")

        cache_info = VTSCacheInfo(
            days_times_mapping=days_times_mapping,
            dt_mapping_pcount=dt_mapping_pcount,
            copy_mode=False,
            show_n_aval_players=False,
            team_id=registered_team.id,
            min_players=min_players,
            duration=duration,
            total_pages=math.ceil(len(days_times_mapping) / self.PAGE_SIZE),
            current_page=0,
        )

        self.cache[ctx.author_id, ctx.channel_id] = cache_info

        content = await self.create_view_message(cache_info=cache_info)

        await ctx.send(content=content,
                       hidden=True,
                       components=await self.view_timeslots_components(cache_info=cache_info))

    @MyCoggy.component_callback(components=component_id.viewall_copy_button)
    async def view_timeslots_copy_button_callback(self, ctx: ComponentContext):
        cache_info = self.cache[ctx.author_id, ctx.channel_id]
        cache_info.copy_mode = not cache_info.copy_mode

        content = await self.create_view_message(cache_info=cache_info)
        await ctx.edit_origin(content=content, components=await self.view_timeslots_components(cache_info=cache_info))

    @MyCoggy.component_callback(components=component_id.number_of_players_button)
    async def view_timeslots_number_of_players_button_callback(self, ctx: ComponentContext):
        cache_info = self.cache[ctx.author_id, ctx.channel_id]
        cache_info.show_n_aval_players = not cache_info.show_n_aval_players

        content = await self.create_view_message(cache_info=cache_info)
        await ctx.edit_origin(content=content, components=await self.view_timeslots_components(cache_info=cache_info))

    @MyCoggy.component_callback(components=component_id.view_timeslot_pages_select)
    async def view_timeslot_pages_select(self, ctx: ComponentContext):
        cache_info = self.cache[ctx.author_id, ctx.channel_id]
        cache_info.current_page = int(ctx.selected_options[0])

        content = await self.create_view_message(cache_info=cache_info)
        await ctx.edit_origin(content=content, components=await self.view_timeslots_components(cache_info=cache_info))


def setup(slash: SlashCommand, bot: discord.Client):
    Propose().setup(slash=slash, bot=bot)
    ViewTimeSlots().setup(slash=slash, bot=bot)
