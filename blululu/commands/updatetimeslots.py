from __future__ import annotations

import dataclasses
from collections.abc import Iterable
from datetime import datetime, timedelta, timezone
from pprint import pprint
from typing import Any, Callable, Optional, Awaitable

import discord
from discord_slash import SlashContext, SlashCommand, ButtonStyle, ComponentContext, SlashCommandOptionType
from discord_slash.utils.manage_commands import create_option
from discord_slash.utils.manage_components import create_button, create_select, create_select_option, create_actionrow

from blululu.commands._utils import CacheManager, MatchProposalMessageHandler, ComponentsIDs, MyCoggy, \
    ThreadPartialMessage, please_give_me_the_thread, please_give_me_the_channel, HelpMessage, HelpMessageCommandExample
from blululu.logger import logger


@dataclasses.dataclass
class CacheInfo:
    days_times_mapping: dict[datetime, dict[timedelta, bool]] = dataclasses.field(default_factory=dict)
    selected_days: set[datetime] = dataclasses.field(default_factory=set)
    is_times_focused: bool = dataclasses.field(default=False)


class TimeSelector(MyCoggy):
    component_id = ComponentsIDs()

    time_emojis = ["🕛", "🕐", "🕑", "🕒", "🕓", "🕔", "🕕", "🕖", "🕗", "🕘", "🕙", "🕚"]

    def __init__(self, allow_multi_choice: bool = True,
                 get_components_callback: Optional[Callable[[int], Awaitable[list[dict]]]] = None):
        self.cache: CacheManager[int, CacheInfo] = CacheManager()
        self.allow_multi_choice = allow_multi_choice
        self.get_components_callback = get_components_callback

    async def get_selection_buttons_action_row(self):
        buttons = [
            create_button(
                style=ButtonStyle.green,
                label="focused timeslots",
                custom_id=self.component_id.focused_timeslots(self)
            ),
            create_button(
                style=ButtonStyle.blue,
                label="broader timeslots",
                custom_id=self.component_id.broader_timeslots(self)
            )
        ]

        return create_actionrow(*buttons)

    async def get_week_action_row(self, cache_key: Any) -> dict:
        cache_info = self.cache[cache_key]
        days = []
        logger.debug(f"{cache_info.days_times_mapping=}")
        for dt in cache_info.days_times_mapping:
            days.append((dt, dt in cache_info.selected_days))
        logger.debug(f"{days=}")
        select = create_select(
            custom_id=self.component_id.day_of_the_week(self),
            options=[
                create_select_option(label=dt.strftime("%A (%d/%b)"), value=dt.isoformat(), default=default)
                for (dt, default) in days],
            placeholder="Choose your option",  # the placeholder text to show when no options have been chosen
            min_values=1,  # the minimum number of options a user must select
            max_values=len(days) if self.allow_multi_choice else 1,  # the maximum number of options a user can select
        )
        return create_actionrow(select)

    @staticmethod
    def padding(t, left_zeros):
        t = str(t)
        return '0' * (left_zeros - len(t)) + t

    def generate_time_option(self, t: float, default: bool = False):
        hours = int(t)
        minutes = int((t - int(t)) * 60)
        formatted_time = f"{self.padding(hours, 2)}:{self.padding(minutes, 2)}"
        return create_select_option(formatted_time, value=str(t), emoji=self.time_emojis[int(t) % 12], default=default)

    @staticmethod
    def get_timeslot_cost(timeslots: set[float]) -> list[int]:
        timeslots_importance = [0] * 48
        for i in timeslots:
            i = int(i * 2)
            for j in range(0, 24):
                timeslots_importance[(i + j) % 48] += j
            for j in range(1, 24 + 1):
                timeslots_importance[(i - j) % 48] += j

        return timeslots_importance

    async def get_timeslot_action_row(self, cache_key: Any) -> Optional[dict]:
        cache_info = self.cache[cache_key]
        focused = cache_info.is_times_focused
        cached_times: set[timedelta] = set()
        cached_times_all_options: set[timedelta] = set()
        for i, day in enumerate(cache_info.selected_days):
            times = set(hour for hour in cache_info.days_times_mapping[day] if cache_info.days_times_mapping[day][hour])
            all_options = set(cache_info.days_times_mapping[day].keys())
            if i == 0:
                cached_times = times
                cached_times_all_options = all_options
            else:
                cached_times.intersection_update(times)
                cached_times_all_options.intersection_update(all_options)
        cached_times_hours = set(i.seconds / 3600 for i in cached_times)
        cached_times_all_options_hours = set(i.seconds // 3600 for i in cached_times_all_options)
        cached_times_all_options_half_accurate = set(i.seconds / 3600 for i in cached_times_all_options)
        if focused:
            cached_times_hours_focused: set[float] = set()
            if self.allow_multi_choice:
                hour_cost = self.get_timeslot_cost(cached_times_hours)
                for i2 in cached_times_hours:
                    cached_times_hours_focused.add(i2)
                    cached_times_hours_focused.add(i2 + 0.5)
                    cached_times_hours_focused.add(i2 - 0.5)

                for h in sorted(range(48), key=lambda x: hour_cost[x]):
                    cached_times_hours_focused.add(h / 2)
                    if len(cached_times_hours_focused) == 24:  # 24 is the discord limit
                        break
            else:
                hour_cost = self.get_timeslot_cost(cached_times_all_options_half_accurate)
                for h2 in sorted(cached_times_all_options_half_accurate, key=lambda x: hour_cost[int(x * 2)]):
                    cached_times_hours_focused.add(h2)
                    if len(cached_times_hours_focused) == 24:  # 24 is the discord limit
                        break

            logger.debug(cached_times_hours_focused)
            options = [self.generate_time_option(i, default=i in cached_times_hours) for i in
                       sorted(cached_times_hours_focused)]
        else:
            options = [self.generate_time_option(i, default=i in cached_times_hours) for i in
                       cached_times_all_options_hours]
        logger.debug(f"{len(options)=}, {cached_times_all_options_hours=} {options=}")
        if len(options) == 0:
            return None

        select = create_select(
            custom_id=self.component_id.timeslots(self),
            options=options,
            placeholder="Choose your option",  # the placeholder text to show when no options have been chosen
            min_values=0,  # the minimum number of options a user must select
            max_values=max(len(options), 1) if self.allow_multi_choice else 1,
            # the maximum number of options a user can select
        )
        return create_actionrow(select)

    async def get_all_components(self, author_id: int):
        components = []
        components.append(await self.get_selection_buttons_action_row())
        components.append(await self.get_week_action_row(author_id))
        components.append(await self.get_timeslot_action_row(author_id))
        if self.get_components_callback is not None:
            components.extend(await self.get_components_callback(author_id))
        return [c for c in components if c is not None]

    @MyCoggy.component_callback(components=component_id.focused_timeslots)
    async def focused_timeslots(self, ctx: ComponentContext):
        cache_info = self.cache[ctx.author_id]
        cache_info.is_times_focused = True
        self.cache[ctx.author_id] = cache_info
        logger.debug(f"{cache_info.is_times_focused=}")
        await ctx.edit_origin(components=await self.get_all_components(ctx.author_id))

    @MyCoggy.component_callback(components=component_id.broader_timeslots)
    async def broader_timeslots(self, ctx: ComponentContext):
        self.cache[ctx.author_id].is_times_focused = False
        await ctx.edit_origin(components=await self.get_all_components(ctx.author_id))

    @MyCoggy.component_callback(components=component_id.day_of_the_week)
    async def set_day_of_the_week(self, ctx: ComponentContext):
        selected_dates = set(map(datetime.fromisoformat, ctx.selected_options))
        await self.select_days(ctx.author_id, selected_dates)

        await ctx.edit_origin(components=await self.get_all_components(ctx.author_id))

    @MyCoggy.component_callback(components=component_id.timeslots)
    async def set_timeslots(self, ctx: ComponentContext):
        cache_info = self.cache[ctx.author_id]
        selected_hours = set(map(lambda x: timedelta(hours=float(x)), ctx.selected_options))
        for day in cache_info.selected_days:
            for timeslot in cache_info.days_times_mapping[day]:
                if not cache_info.is_times_focused and timeslot.seconds % 3600 != 0:
                    # the condition bellow is for when you unselect an hour, in the unfocused mode, it should unselect
                    # its half hour. i hate this project.
                    if (timeslot - timedelta(minutes=30)) not in selected_hours:
                        cache_info.days_times_mapping[day][timeslot] = False
                    continue
                cache_info.days_times_mapping[day][timeslot] = timeslot in selected_hours

        for day in cache_info.selected_days:
            for timeslot in cache_info.days_times_mapping[day]:
                # i wrote this down but i really forgot what it is for.
                # TODO: try to comprehend the things bellow, most likely it is something to do with selecting an hour
                #   near the end of the day and it tries to get the next half hour in the next day??? why life is
                #   so complicated like that?
                if not cache_info.is_times_focused \
                        and cache_info.days_times_mapping[day][timeslot] \
                        and timeslot.seconds % 3600 == 0:
                    if timeslot.seconds <= (22 * 3600):
                        if cache_info.days_times_mapping[day].get(timeslot + timedelta(hours=1), False):
                            cache_info.days_times_mapping[day][timeslot + timedelta(hours=0.5)] = True
                    elif (day_plus_one := day + timedelta(days=1)) in cache_info.days_times_mapping:
                        dt = timedelta(hours=(timeslot.seconds / 3600 + 24 + 1) % 24)
                        if cache_info.days_times_mapping[day_plus_one][dt]:
                            dt2 = timedelta(hours=(timeslot.seconds / 3600 + 24 + 0.5) % 24)
                            cache_info.days_times_mapping[day_plus_one][dt2] = True
                    else:
                        logger.debug("bobo")

        self.cache[ctx.author_id] = cache_info
        await ctx.edit_origin(components=await self.get_all_components(ctx.author_id))

    # async def set_days_times_mapping(self, dtm: dict[datetime, dict[timedelta, bool]], author_id: int) -> None:
    #     self.cache[author_id].days_times_mapping = dtm

    async def set_days_times_mapping(self, dtm: dict[datetime, bool], author_id: int) -> None:
        self.cache[author_id] = CacheInfo()
        days_times_mapping: dict[datetime, dict[timedelta, bool]] = dict()
        for dt, c in dtm.items():
            day = datetime(year=dt.year, month=dt.month, day=dt.day, tzinfo=dt.tzinfo)
            if day not in days_times_mapping:
                days_times_mapping[day] = dict()
            days_times_mapping[day][dt - day] = c
        self.cache[author_id].days_times_mapping = days_times_mapping
        logger.debug(f"{self.cache[author_id].days_times_mapping=}")

    async def get_days_times_mapping(self, author_id: int) -> dict[datetime, dict[timedelta, bool]]:
        return self.cache[author_id].days_times_mapping

    async def delete_cache(self, author_id: int) -> None:
        del self.cache[author_id]

    async def select_days(self, author_id: int, days: Iterable[datetime]):
        self.cache[author_id].selected_days = set(days)

    async def get_selected_days_and_times(self, author_id: int) -> Optional[datetime]:
        selected_stuff = []
        for day in self.cache[author_id].selected_days:
            for timeslot, is_selected in self.cache[author_id].days_times_mapping[day].items():
                if is_selected:
                    selected_stuff.append(day + timeslot)
        logger.debug(f"{selected_stuff=}")
        if len(selected_stuff) > 1:
            raise ValueError("this function is not intended to be run in abgouis situations where there are many "
                             "possible returns. in simple terms `allow_multi_choice` should be false in __init__ "
                             "for this function to work as expected")
        return selected_stuff[0] if len(selected_stuff) == 1 else None


class UpdateTimeSlots(TimeSelector):
    component_id = ComponentsIDs()

    def __init__(self, allow_multi_choice: bool = True,
                 get_components_callback: Optional[Callable[[int], Awaitable[list[dict]]]] = None):
        super().__init__(allow_multi_choice, get_components_callback)
        self.update_timeslots_mode_cache: CacheManager[int, bool] = CacheManager()

    async def get_time_options_from_db(self, user_id: int, include_maybe: bool) -> dict[datetime, bool]:
        from blululu.tables import async_engine, TimeSlot, User
        from blululu.tables import CanYouPlay
        from sqlmodel.ext.asyncio.session import AsyncSession
        from sqlmodel import select

        async with AsyncSession(async_engine) as session:
            statement = select(TimeSlot).where(
                TimeSlot.user_id == user_id,
                # this condition will not yield result for the current exact timeslot
                # (aka the time is 12:15 but the timeslot 12:00 will not appear even thought that time slot covers
                # 12:00 - 12:30. but i honestly dont care)
                # one hour later: actually i care, timedelta would be enough to fix it
                TimeSlot.date >= (datetime.now(timezone.utc) - timedelta(minutes=30)),
                (datetime.now(timezone.utc) + timedelta(weeks=1)) >= TimeSlot.date
                # (TimeSlot.can_play == CanYouPlay.YES) | (TimeSlot.can_play == CanYouPlay.MAYBE)
            )
            result: list[TimeSlot] = (await session.exec(statement)).all()  # type: ignore
            user: User = (await session.exec(select(User).where(User.id == user_id))).one()  # type: ignore
            user_timezone = user.timezone
            res: dict[datetime, bool] = dict()
            # code 100
            for ts in result:
                d = ts.date.astimezone(tz=timezone(offset=timedelta(hours=user_timezone)))
                p = ts.can_play is CanYouPlay.YES
                if include_maybe:
                    p |= ts.can_play is CanYouPlay.MAYBE  # i know this looks fancy, thank you.

                res[d] = p
        logger.debug(f"{res=}")
        return res
        #     dates = [i.date for i in result]
        #     correct_timezone = [i.astimezone(tz=timezone(offset=timedelta(hours=user_timezone))) for i in dates]
        #
        # current_time = datetime.now(tz=timezone(offset=timedelta(hours=user_timezone)))
        # current_time_limited = datetime(year=current_time.year, month=current_time.month, day=current_time.day,
        #                                 tzinfo=current_time.tzinfo)
        # days_times_mapping = dict()

        # for half_hours in range(2*24*7):
        #     dt = current_time_limited + timedelta(minutes=30*half_hours)
        #     days_times_mapping[dt] = False
        #
        # for dt in correct_timezone:
        #     days_times_mapping[dt] = True
        #
        # return days_times_mapping

    async def ignore(self, ctx: ComponentContext):
        await ctx.defer(ignore=True)

    async def get_submit_action_row(self):
        buttons = [
            create_button(
                style=ButtonStyle.primary,
                label="submit",
                custom_id=self.component_id.submit(self)
            ),
            create_button(
                style=ButtonStyle.red,
                label="reset",
                custom_id=self.component_id.reset_timeslots(self)
            )
        ]
        return create_actionrow(*buttons)

    async def get_all_components(self, author_id: int):
        components = await super().get_all_components(author_id)
        components.append(await self.get_submit_action_row())
        return components

    @MyCoggy.component_callback(components=component_id.reset_timeslots)
    async def reset_timeslots(self, ctx: ComponentContext):
        await self.delete_cache(ctx.author_id)
        include_maybe = self.update_timeslots_mode_cache[ctx.author_id]
        await self.set_days_times_mapping(await self.get_time_options_from_db(ctx.author_id, include_maybe),
                                          ctx.author_id)
        logger.debug("reeessseeesssettt")
        await ctx.edit_origin(components=await self.get_all_components(ctx.author_id))

    @MyCoggy.component_callback(components=component_id.submit)
    async def submit(self, ctx: ComponentContext):
        await ctx.defer(edit_origin=True)
        await ctx.edit_origin(components=[], content='The submission is being processed. Please wait.')
        from blululu.tables import async_engine, TimeSlot
        from blululu.tables import CanYouPlay, MatchProposal, Match, MatchProposalThread, RegisteredTeam
        from sqlmodel.ext.asyncio.session import AsyncSession
        # from sqlmodel import select
        days_times_mapping = await self.get_days_times_mapping(ctx.author_id)
        # match_proposals_timeslot_pairs: dict[TimeSlot, list[MatchProposal]] = dict()
        match_proposals: dict[int, MatchProposal] = dict()
        cached_matches: dict[int, Match] = dict()
        cached_match_proposal_thread: dict[int, MatchProposalThread] = dict()
        cached_registered_team: dict[int, RegisteredTeam] = dict()
        all_timeslots: list[TimeSlot] = []
        async with AsyncSession(async_engine, expire_on_commit=False) as session:
            for day in days_times_mapping:
                for timeslot in days_times_mapping[day]:
                    ts = TimeSlot(
                        user_id=ctx.author_id,
                        date=day + timeslot,
                        can_play=CanYouPlay.YES if days_times_mapping[day][timeslot] else CanYouPlay.NO)
                    ts = await session.merge(ts)
                    all_timeslots.append(ts)
            await session.commit()

            # the down part is for updating the vote on the match that were already proposed
            for ts in all_timeslots:
                await session.refresh(ts)
                # match_proposals_timeslot_pairs[ts] = []
                mps: list[MatchProposal] = await session.run_sync(lambda _session: ts.match_proposals)
                for mp in mps:
                    assert mp.id is not None
                    match_proposals[mp.id] = mp
                # for mp in mps:
                #     match_proposals_timeslot_pairs[ts].append(mp)
                # match_proposals_timeslot_pairs.append((mp, ts))

            logger.debug(f"{match_proposals=}")
            changed_mps: list[tuple[Match, ThreadPartialMessage]] = []
            for mp in match_proposals.values():
                # for mp in mp_list:
                # this caching shit might not be necessary if sqlalchemy is smart enough but at this point i dont
                # trust anything it does
                if mp.match_id not in cached_matches:
                    cached_matches[mp.match_id] = await session.run_sync(lambda _session: mp.match)
                match = cached_matches[mp.match_id]
                if match.is_archived:
                    continue
                assert match.id is not None
                if match.id not in cached_match_proposal_thread:
                    cached_match_proposal_thread[match.id] = await session.run_sync(
                        lambda _session: match.match_proposal_thread)

                if match.id not in cached_registered_team:
                    cached_registered_team[match.id] = await session.run_sync(
                        lambda _s: match.proposerer_team
                    )
                my_registered_team = cached_registered_team[match.id]
                match_proposal_thread = cached_match_proposal_thread[match.id]
                logger.debug(match_proposal_thread)
                match_proposals_channel = await please_give_me_the_thread(
                    ctx=ctx,
                    thread_id=match_proposal_thread.id,
                    channel=await please_give_me_the_channel(
                        ctx=ctx,
                        channel_id=my_registered_team.discord_channel_proposals_id
                    )
                )
                logger.debug(f"{match_proposals_channel=}")
                message = ThreadPartialMessage(channel=match_proposals_channel, id=mp.message_id)
                timeslots: list[TimeSlot] = await session.run_sync(lambda _s: mp.timeslots)
                timeslots = [i for i in timeslots if i.user_id == ctx.author_id]
                playability = await MatchProposalMessageHandler.convert_rows_to_playability(timeslots)
                can_play = playability[ctx.author_id]
                """
                this will raise an error when
                1 - can_play is YES
                2 - the match_id in the timeslot is different than the current match_id for the proposal
                
                match_id in the TimeSlot can only hold one value, but for the match duration, multiple timeslots can
                hold different match_id for confirmed matches.
                when you click "yes, i can play" bla bla it will change the timeslot vote so that all the timeslot are
                CanYouPlay.YES. but here when someone does the update for the timeslot, it doesnt have to be continues.
                
                idk man, i just hope MatchProposalMessageHandler.convert_rows_to_playability does its job and set stuff
                to CanYouPlay.MAYBE when things are ambiguous so things dont break.
                """
                has_changed = await MatchProposalMessageHandler.better_update_can_play(
                    session=session,
                    can_play=can_play,
                    origin_message=message,
                    ctx=ctx,
                    author_id=ctx.author_id,
                    update_timeslots=False
                )
                if has_changed:
                    changed_mps.append((match, message))

        await self.delete_cache(author_id=ctx.author_id)
        if len(changed_mps) > 0:
            content = "these matches have changed due to the time slots update and i tried to guess your availability." \
                      " you can check them." + '\n'.join(m.jump_url for _, m in changed_mps)
        else:
            content = ''
        logger.debug(f"{content=}")
        await ctx.send(content='Succeeded, Thank you come again!\n' + content, hidden=True)
        # await self.ignore(ctx)


    updatetimeslots_help_message = HelpMessage(
        description='to update the timeslots availability',
        examples=[
            HelpMessageCommandExample(
                goal='I would like to update my timeslots since i have moved to another city or my school times has '
                     'changed',
                command='/update-timeslots'
            ),
            HelpMessageCommandExample(
                goal='In `/propose` and `/view-timeslots` commands the "complete" mode shows me as "Im not sure" and'
                     'I would like to check all the times that the bot thinks im "Im not sure" at.',
                command='/update-timeslots model:complete'
            ),
            HelpMessageCommandExample(
                goal='I want to minimize the "Im not sure" auto position given by the bot to me by removing every '
                     '"Im not sure" position and let me review my availability again',
                command='/update-timeslots model:confident'
            )
        ]
    )

    @MyCoggy.slash(name='update-timeslots',
                   description='to update your timeslots availability',
                   options=[
                        create_option(
                            name="mode",
                            description="choose the mode for what to prioritize when prefilling the timeslots menus. "
                                        "(default: complete)",
                            choices=[dict(name="complete", value=1), dict(name="confident", value=0)],
                            required=False,
                            option_type=SlashCommandOptionType.INTEGER
                        )
                  ],
                   default_permission=False,
                   command_group='after_config_user'
                   )
    async def UpdateTimeSlots(self, ctx: SlashContext, mode: int = 1):
        include_maybe = bool(mode)
        self.update_timeslots_mode_cache[ctx.author_id] = include_maybe
        logger.debug("UpdateTimeSlots1")
        logger.debug(f"{ctx.author_id=}")
        await self.set_days_times_mapping(await self.get_time_options_from_db(ctx.author_id, include_maybe),
                                          author_id=ctx.author_id)
        await ctx.send(content="Choose your timeslots which you are available at.",
                       components=await self.get_all_components(ctx.author_id), hidden=True)

    @MyCoggy.loop(minutes=30)
    async def guess_update_future_timeslots(self):
        logger.debug("processing")
        from blululu.tables import async_engine, TimeSlot
        from blululu.tables import CanYouPlay
        from sqlmodel import select
        from sqlmodel.ext.asyncio.session import AsyncSession

        identity_type = tuple[int, int, int]

        async with AsyncSession(async_engine) as session:
            statement = select(TimeSlot).where(TimeSlot.date >= (datetime.now(timezone.utc) - timedelta(weeks=3)))
            all_timeslots: list[TimeSlot] = (await session.exec(statement)).all()
            base_datetime = datetime.now(timezone.utc)
            base_datetime -= timedelta(days=base_datetime.weekday(),
                                       hours=base_datetime.hour,
                                       minutes=base_datetime.minute,
                                       seconds=base_datetime.second,
                                       microseconds=base_datetime.microsecond
                                       )

            def gen_date_identity(date: datetime) -> identity_type:
                return date.weekday(), date.hour, date.minute

            user_groups: dict[int, dict[identity_type, list[TimeSlot]]] = dict()
            latest_timeslot_per_user: dict[int, TimeSlot] = dict()
            for timeslot in all_timeslots:
                if timeslot.user_id not in user_groups:
                    user_groups[timeslot.user_id] = dict()
                identity = gen_date_identity(timeslot.date)
                if identity not in user_groups[timeslot.user_id]:
                    user_groups[timeslot.user_id][identity] = list()
                user_groups[timeslot.user_id][identity].append(timeslot)

                if timeslot.user_id not in latest_timeslot_per_user:
                    latest_timeslot_per_user[timeslot.user_id] = timeslot

                if timeslot.date > latest_timeslot_per_user[timeslot.user_id].date:
                    latest_timeslot_per_user[timeslot.user_id] = timeslot

            for minutes in range(0, 60 * 24 * 7, 30):
                dt = base_datetime + timedelta(minutes=minutes)
                identity = gen_date_identity(dt)
                for user_id in list(user_groups.keys()):
                    if identity not in user_groups[user_id]:
                        user_groups[user_id][identity] = []

            predictions: dict[int, dict[identity_type, CanYouPlay]] = dict()
            for user_id, timeslot_identities in user_groups.items():
                if user_id not in predictions:
                    predictions[user_id] = dict()

                for identity, timeslots in timeslot_identities.items():
                    if len(timeslots) > 0:
                        can_you_play = await MatchProposalMessageHandler.convert_rows_to_playability(timeslots)
                        predictions[user_id][identity] = can_you_play[user_id]
                    else:
                        predictions[user_id][identity] = CanYouPlay.NO


            new_rows: list[TimeSlot] = []
            for user_id, time_can_you_play in predictions.items():
                for (weekday, hour, minute), can_you_play in time_can_you_play.items():
                    date = base_datetime + timedelta(weeks=1, days=weekday, hours=hour, minutes=minute)
                    new_rows.append(TimeSlot(user_id=user_id, date=date, can_play=can_you_play))

            logger.debug(f"{len(new_rows)=}")

            for row in new_rows:
                await session.merge(row)

            await session.commit()


def setup(slash: SlashCommand, bot: discord.Client):
    UpdateTimeSlots().setup(slash=slash, bot=bot)
