from __future__ import annotations

import asyncio
import dataclasses
import functools
import inspect
import random
import sys
from abc import ABC, abstractmethod
from collections.abc import MutableMapping
from datetime import datetime, timedelta, timezone
from enum import Enum
from typing import TypeVar, Iterator, Callable, Optional, Union, Any, Sequence, Literal, TypedDict, Protocol, overload

from blululu.logger import logger
import aiohttp
import discord
from discord import TextChannel, Role, Message, PartialMessage, Thread, DMChannel, Guild, Member
from discord import utils
from discord.abc import Snowflake
from discord.ext import tasks
from discord_slash import ButtonStyle, ComponentContext, SlashCommand, SlashContext, MenuContext
from discord_slash.model import BaseCommandObject, SubcommandObject, SlashMessage
from discord_slash.utils.manage_components import create_button, create_actionrow
from sqlalchemy import false, true
from sqlalchemy.ext.asyncio.session import AsyncSession as sa_AsyncSession
from sqlmodel.ext.asyncio.session import AsyncSession

from blululu.config import debug_guilds
from blululu.erros import SlashCommandException, slash_error_handler
from blululu.tables import Team, RegisteredTeam, MatchProposal, MatchProposalThread, TimeSlot, MatchProposalVotes, \
    CanYouPlay, MatchDiscussionThread, Match


# i hate discord.py
class ThreadPartialMessage(PartialMessage):
    def __init__(self, *, channel: Union[TextChannel, DMChannel, Thread], id: int):  # noqa
        # if channel.type not in (ChannelType.text, ChannelType.news, ChannelType.private):
        #     raise TypeError(f'Expected TextChannel or DMChannel not {type(channel)!r}')

        self.channel = channel
        self._state = channel._state
        self.id = id


class GenCompIDs(Enum):
    def _generate_next_value_(name: str, start, count, last_values):  # type: ignore
        return f"{__name__}_{name.lower()}"


_K = TypeVar("_K")
_V = TypeVar("_V")


class CacheManager(MutableMapping[_K, _V]):
    def __init__(self,
                 delete_after: timedelta = timedelta(minutes=10),
                 safe_checks: bool = True):
        self.cache: dict[_K, _V] = dict()
        self.last_time_used: dict[_K, datetime] = dict()
        self.delete_after: timedelta = delete_after
        self.safe_checks = safe_checks

    def __setitem__(self, k: _K, v: _V) -> None:
        self.check_if_is_old(k)
        self.cache[k] = v
        self.last_time_used[k] = datetime.now()

    def __delitem__(self, k: _K) -> None:
        try:
            del self.cache[k]
            del self.last_time_used[k]
        except KeyError:
            pass

    def __getitem__(self, k: _K) -> _V:
        if self.safe_checks and k not in self.cache:
            raise SlashCommandException(
                message='Honestly, i dont know what happen to lead to this error. '
                        'In any case, try to run the command again, maybe by the power of gods it will work?!',
                remove_embed=True,
                remove_components=True
            )
        self.check_if_is_old(k)
        self.last_time_used[k] = datetime.now()
        return self.cache[k]

    def __len__(self) -> int:
        return len(self.cache)

    def __iter__(self) -> Iterator[_K]:
        return iter(self.cache)

    def check_if_is_old(self, k: _K):
        current_time = datetime.now()
        if k in self.last_time_used:
            is_old = (current_time - self.last_time_used[k]) > self.delete_after
        else:
            is_old = False
        self.delete_old()
        if is_old:
            raise SlashCommandException(
                message="Please try to run the command again, it will work, trust me.",
                remove_components=True,
                remove_embed=True
            )

    def delete_old(self):
        current_time = datetime.now()
        for key, last_time in list(self.last_time_used.items()):
            if (current_time - last_time) > self.delete_after:
                del self[key]


class ComponentIDFuture:
    def __init__(self, name: str):
        self.name = name

    def __call__(self, object: MyCoggy):
        h = object.__class__.__qualname__  # hex(hash(cls))
        return f"{self.name}_{h}_{object._my_count}"


class ComponentsIDs:
    def __init__(self):
        self.ids = dict()
        self.inited = False

    def init(self):
        self.inited = True

    def __getattr__(self, name: str):
        return ComponentIDFuture(name)


_A_COGGY = TypeVar("_A_COGGY", bound="MyCoggy")


def run_slash(slash_func: Callable[[Union[SlashCommand, SlashContext]], Any]):
    def slash_wrapper(func):
        func._slash_func = slash_func
        return func

    return slash_wrapper


discord_snowflake_type = Union[int, str]  # i have no idea what snowflake is.


class CmdPermission(TypedDict):
    id: discord_snowflake_type
    type: Literal[1, 2]
    permission: bool


class CmdGuildPermissions(TypedDict):
    id: discord_snowflake_type
    application_id: discord_snowflake_type
    guild_id: discord_snowflake_type
    permissions: list[CmdPermission]


class CmdObject(TypedDict):
    # https://discord.com/developers/docs/interactions/application-commands#application-command-object-application-command-structure
    id: discord_snowflake_type
    type: Optional[Literal[1, 2, 3]]
    application_id: discord_snowflake_type
    guild_id: Optional[discord_snowflake_type]
    name: str
    description: str
    options: Optional[list[dict]]
    default_permission: Optional[bool]
    version: discord_snowflake_type


class SendProtocol(Protocol):

    async def __call__(self,
                       content: str,
                       embed: discord.Embed = None,
                       components: list[dict] = None,
                       ) -> Union[SlashMessage, dict, discord.Message]:
        pass


class MyCoggy:
    component_id: ComponentsIDs = ComponentsIDs()
    used_coggies: list[MyCoggy] = []
    bot: discord.Client
    slash_client: SlashCommand
    all_slash_commands: dict[str, Union[BaseCommandObject, SubcommandObject]] = dict()
    command_groups: dict[str, dict[str, Union[BaseCommandObject, SubcommandObject]]] = dict()
    help_messages: dict[str, HelpMessage] = dict()
    _existing_cmds: dict[int, list[CmdObject]] = dict()
    _counter: int = 0
    _my_count: int

    def __new__(cls, *args, **kwargs) -> MyCoggy:
        o = super().__new__(cls)
        o._my_count = o._counter
        cls._counter += 1
        return o

    def __init_subclass__(cls, **kwargs) -> None:
        super().__init_subclass__(**kwargs)
        cls.used_coggies = []

    @staticmethod
    def slash(*,
              name: str = None,
              description: str = None,
              guild_only: bool = True,
              options: list[dict] = None,
              default_permission: bool = True,
              permissions: dict = None,
              connector: dict = None,
              command_group: Optional[str] = None,
              help_message: Optional[HelpMessage] = None
              ):
        if guild_only:
            guild_ids = debug_guilds
        else:
            guild_ids = None
        kwargs = dict(name=name, description=description, guild_ids=guild_ids, options=options,
                      default_permission=default_permission, permissions=permissions, connector=connector
                      )

        def slash_wrapper(func):
            func._slash_kwargs = kwargs
            func._mycoggy_command_group = command_group
            func._mycoggy_help_message = help_message
            return func

        return slash_wrapper

    @staticmethod
    def subcommand(
            *,
            base,
            subcommand_group=None,
            name=None,
            description: str = None,
            base_description: str = None,
            base_desc: str = None,
            base_default_permission: bool = True,
            base_permissions: dict = None,
            subcommand_group_description: str = None,
            sub_group_desc: str = None,
            guild_only: bool = True,
            options: list[dict] = None,
            connector: dict = None,
            command_group: Optional[str] = None,
            help_message: Optional[HelpMessage] = None
    ):
        if guild_only:
            guild_ids = debug_guilds
        else:
            guild_ids = None
        kwargs = dict(
            base=base,
            subcommand_group=subcommand_group,
            name=name,
            description=description,
            base_description=base_description,
            base_desc=base_desc,
            base_default_permission=base_default_permission,
            base_permissions=base_permissions,
            subcommand_group_description=subcommand_group_description,
            sub_group_desc=sub_group_desc,
            guild_ids=guild_ids,
            options=options,
            connector=connector)

        def subcommand_wrapper(func):
            func._subcommand_kwargs = kwargs
            func._mycoggy_command_group = command_group
            func._mycoggy_help_message = help_message
            return func

        return subcommand_wrapper

    @staticmethod
    def component_callback(
            *,
            messages: Union[int, discord.Message, list] = None,
            components: Union[str, dict, list] = None,
            use_callback_name=True,
            component_type: int = None,
    ):
        kwargs = dict(messages=messages, components=components, use_callback_name=use_callback_name,
                      component_type=component_type)

        def component_callback_wrapper(func):
            func._component_callback_kwargs = kwargs
            return func

        return component_callback_wrapper

    @staticmethod
    def loop(
            *,
            seconds: float = utils.MISSING,
            minutes: float = utils.MISSING,
            hours: float = utils.MISSING,
            time: Union[datetime, Sequence[datetime]] = utils.MISSING,
            count: Optional[int] = None,
            reconnect: bool = True,
            loop: Optional[asyncio.AbstractEventLoop] = None,
    ):
        kwargs = dict(seconds=seconds, minutes=minutes, hours=hours, time=time, count=count, reconnect=reconnect,
                      loop=loop)

        def loop_wrapper(func):
            func._loop_kwargs = kwargs
            return func

        return loop_wrapper

    @staticmethod
    def context_menu(
            *,
            target: int,
            name: str,
            guild_only: bool = True,
            help_message: Optional[HelpMessage] = None
    ):
        if guild_only:
            guild_ids = debug_guilds
        else:
            guild_ids = None
        kwargs = dict(target=target, name=name, guild_ids=guild_ids)

        def context_menu_wrapper(func):
            func._context_menu_kwargs = kwargs
            func._mycoggy_help_message = help_message
            return func

        return context_menu_wrapper

    def attach_coggy(self, coggy: _A_COGGY) -> _A_COGGY:
        self.used_coggies.append(coggy)
        return coggy

    def setup(self, slash: SlashCommand, bot: discord.Client):
        for coggy in self.used_coggies:
            coggy.setup(slash=slash, bot=bot)
        self.bot = bot
        self.slash_client = slash
        methods = inspect.getmembers(self, predicate=inspect.ismethod)
        self.component_id.init()
        for name, method in methods:
            if hasattr(method, '_slash_kwargs'):
                o = slash.slash(**method._slash_kwargs)(slash_error_handler(method))
                self.all_slash_commands[o.name] = o
                if method._mycoggy_command_group is not None:
                    if method._mycoggy_command_group not in self.command_groups:
                        self.command_groups[method._mycoggy_command_group] = dict()
                    self.command_groups[method._mycoggy_command_group][o.name] = o
                if method._mycoggy_help_message is not None:
                    self.help_messages[o.name] = method._mycoggy_help_message
                    self.help_messages[o.name].setup(slash=slash, bot=bot)

            elif hasattr(method, '_subcommand_kwargs'):
                logger.debug(f"{name=}, {method=}, {method._subcommand_kwargs=}")

                o = slash.subcommand(**method._subcommand_kwargs)(slash_error_handler(method))
                self.all_slash_commands[o.base] = o
                if method._mycoggy_command_group is not None:
                    if method._mycoggy_command_group not in self.command_groups:
                        self.command_groups[method._mycoggy_command_group] = dict()
                    self.command_groups[method._mycoggy_command_group][o.base] = o

                if method._mycoggy_help_message is not None and o.base not in self.help_messages:
                    self.help_messages[o.base] = method._mycoggy_help_message
                    self.help_messages[o.base].setup(slash=slash, bot=bot)

            elif hasattr(method, '_component_callback_kwargs'):
                kwargs = method._component_callback_kwargs.copy()
                components_id = kwargs['components']
                if components_id is not None:
                    kwargs['components'] = components_id(self)
                slash.component_callback(**kwargs)(slash_error_handler(method))

            elif hasattr(method, '_loop_kwargs'):
                tasks.loop(**method._loop_kwargs)(method).start()

            elif hasattr(method, '_context_menu_kwargs'):
                o = slash.context_menu(**method._context_menu_kwargs)(slash_error_handler(method))
                if method._mycoggy_help_message is not None:
                    self.help_messages[o.name] = method._mycoggy_help_message
                    self.help_messages[o.name].setup(slash=slash, bot=bot)

    @classmethod
    def create_help_message(cls, slash: SlashCommand, bot: discord.Client):
        for command_name, help_message in cls.help_messages.items():
            slash.subcommand(base='help',
                             name=command_name,
                             guild_ids=debug_guilds,
                             description=f"for helpful information and examples for {command_name} command",
                             )(help_message.help)

    async def get_all_commands(self, guild_id: Optional[int] = None) -> list[CmdObject]:
        return await self.slash_client.req.get_all_commands(guild_id=guild_id)

    async def get_all_guild_command_permissions(self, guild_id: int) -> list[CmdGuildPermissions]:
        return await self.slash_client.req.get_all_guild_commands_permissions(guild_id=guild_id)

    async def update_command_permission(self,
                                        command_name: str,
                                        guild_id: int,
                                        permissions: list[CmdPermission],
                                        all_guild_command_permissions: list[CmdGuildPermissions],
                                        add_permissions: bool = False,
                                        pop_permissions: bool = False):
        if guild_id not in self._existing_cmds:
            self._existing_cmds[guild_id] = await self.get_all_commands(guild_id=guild_id)
            self._existing_cmds[guild_id].extend(await self.get_all_commands())

        logger.debug(f"{self._existing_cmds[guild_id]=}")
        for command in self._existing_cmds[guild_id]:
            if command['name'] == command_name:
                break
        else:
            raise ValueError(f"{command_name} command could not be found")

        if add_permissions or pop_permissions:
            for perm in all_guild_command_permissions:
                if perm['id'] == command['id']:
                    extra_perms = perm['permissions']
                    break
            else:
                extra_perms = []
            if add_permissions:
                new_permissions = extra_perms.copy()
                for per in permissions:
                    if per not in new_permissions:
                        new_permissions.append(per)
                permissions = new_permissions

            if pop_permissions:
                new_permissions = []
                for per in extra_perms:
                    if per not in permissions:
                        new_permissions.append(per)
                permissions = new_permissions

        r = await self.bot.http.edit_application_command_permissions(
            application_id=command['application_id'],
            guild_id=guild_id,
            command_id=command['id'],
            payload=dict(
                permissions=permissions
            )
        )
        logger.debug(f"{r=}")
        return r

    @classmethod
    async def send_random_help_message(cls,
                                       send_func: SendProtocol,
                                       channel_id: int,
                                       prefix: Optional[str] = None) -> Message:
        random_command = random.choice(list(cls.help_messages.values()))
        return await random_command.generate_random_help(
            send_func=send_func,
            channel_id=channel_id,
            prefix=prefix
        )


class HelpMessageExampleAbstract(ABC):
    @abstractmethod
    def generate_embed(self) -> discord.Embed:
        pass


@dataclasses.dataclass
class HelpMessageCommandExample(HelpMessageExampleAbstract):
    goal: str
    command: str

    def generate_embed(self) -> discord.Embed:
        embed = discord.Embed()
        original_command = self.command.split(' ', 1)[0]
        title = f'{original_command} Command'
        embed.title = title
        embed.description = f'goal = {self.goal}\n\n example = `{self.command}`'
        return embed


@dataclasses.dataclass
class HelpMessageMenuExample(HelpMessageExampleAbstract):
    use_case: str
    img_link: Optional[str] = None

    def generate_embed(self) -> discord.Embed:
        embed = discord.Embed()
        title = f'Use Case'
        embed.title = title
        embed.description = self.use_case
        if self.img_link is not None:
            embed.set_image(url=self.img_link)
        return embed


@dataclasses.dataclass
class HelpMessageCacheInfo:
    example_index: int


class HelpMessage(MyCoggy):
    component_id = ComponentsIDs()

    def __init__(self, description: str, examples: list[HelpMessageExampleAbstract]):
        self.description = description
        self.examples = examples

        # 7 days might seems like a big value, but whatever, storing idk how many KB for a week in ram doesnt harm.
        self.cache: CacheManager[Union[tuple[int, int], int], HelpMessageCacheInfo] = CacheManager(
            delete_after=timedelta(days=7)
        )
        self.is_random_help: CacheManager[int, bool] = CacheManager(delete_after=timedelta(days=7), safe_checks=False)

    def add_examples(self, examples: list[HelpMessageExampleAbstract]) -> HelpMessage:
        self.examples.extend(examples)
        return self

    async def create_components(self):
        buttons = [
            create_button(
                style=ButtonStyle.blue,
                label="previous",
                custom_id=self.component_id.previous_button(self)
            ),
            create_button(
                style=ButtonStyle.blue,
                label="next",
                custom_id=self.component_id.next_button(self)
            ),
        ]
        return [create_actionrow(*buttons)]

    async def create_embed(self, cache_info: HelpMessageCacheInfo) -> discord.Embed:
        index = cache_info.example_index % len(self.examples)
        example = self.examples[index]
        embed = example.generate_embed()
        embed.set_footer(text=f'Use Case [{index + 1} of {len(self.examples)}]')
        return embed

    async def _help(self,
                    send_func: SendProtocol,
                    is_random: bool,
                    channel_id: int,
                    author_id: Optional[int] = None,
                    prefix: Optional[str] = None) -> Message:
        content = ''
        if prefix is not None:
            content += prefix + '\n'
        content += self.description
        if is_random:
            example_index = random.randint(0, len(self.examples))
        else:
            example_index = 0
        cache_info = HelpMessageCacheInfo(example_index=example_index)
        if is_random:
            self.cache[channel_id] = cache_info
        else:
            if author_id is None:
                raise ValueError("author_id cannot be None")
            self.cache[author_id, channel_id] = cache_info
        message = await send_func(content=content,
                                  components=await self.create_components(),
                                  embed=await self.create_embed(cache_info))
        if message != {}:
            message_id = message.id  # type: ignore
            self.is_random_help[message_id] = is_random
        return message

    async def help(self, ctx: SlashContext):

        # i know i can use functools.partial, but i dont want to lose the type annotations.
        async def send_func(
                content: str,
                embed: discord.Embed = None,
                components: list[dict] = None,
        ) -> Union[dict, discord.Message]:
            return await ctx.send(content=content, embed=embed, components=components, hidden=True)

        await self._help(send_func=send_func, is_random=False, channel_id=ctx.channel_id, author_id=ctx.author_id)

    async def generate_random_help(self,
                                   send_func: SendProtocol,
                                   channel_id: int,
                                   prefix: Optional[str] = None) -> Message:
        return await self._help(send_func=send_func, is_random=True, prefix=prefix, channel_id=channel_id)

    async def _navigate_callback(self, ctx: ComponentContext, step: int):
        is_random = self.is_random_help.get(ctx.origin_message_id, False)
        cache_info = self.cache[ctx.channel_id] if is_random else self.cache[ctx.author_id, ctx.channel_id]
        cache_info.example_index += step
        await ctx.edit_origin(embed=await self.create_embed(cache_info))

    @MyCoggy.component_callback(components=component_id.previous_button)
    async def previous_button_callback(self, ctx: ComponentContext):
        await self._navigate_callback(ctx, step=-1)

    @MyCoggy.component_callback(components=component_id.next_button)
    async def next_button_callback(self, ctx: ComponentContext):
        await self._navigate_callback(ctx, step=+1)


class SnowFlaky(Snowflake):
    def __init__(self, id: int):
        self.id = id

    @property
    def created_at(self) -> datetime:
        return NotImplemented


async def get_archived_threads(channel: TextChannel, thread_id: int) -> Optional[Thread]:
    async for thread in channel.archived_threads(limit=10):
        if thread.id == thread_id:
            return thread

    return None


@overload
async def please_give_me_the_thread(ctx: Union[ComponentContext, SlashContext, discord.Client],
                                    thread_id: int,
                                    error_check: Literal[True] = True,
                                    force_fetch: bool = False,
                                    channel: Optional[TextChannel] = None
                                    ) -> Thread:
    ...


@overload
async def please_give_me_the_thread(ctx: Union[ComponentContext, SlashContext, discord.Client],
                                    thread_id: int,
                                    error_check: Literal[False],
                                    force_fetch: bool = False,
                                    channel: Optional[TextChannel] = None
                                    ) -> Optional[Thread]:
    ...


async def please_give_me_the_thread(ctx: Union[ComponentContext, SlashContext, discord.Client],
                                    thread_id: int,
                                    error_check: bool = True,
                                    force_fetch: bool = False,
                                    channel: Optional[TextChannel] = None
                                    ) -> Union[Optional[Thread], Thread]:
    if isinstance(ctx, (ComponentContext, SlashContext)):
        thready: Optional[Thread] = ctx.guild.get_thread(thread_id)  # type: ignore
    else:
        thready = None

    if (force_fetch or thready is None) and channel is not None:
        threads = await channel.active_threads()

        for thread in threads:
            if thread.id == thread_id:
                thready = thread

        if thready is None:
            thready = await get_archived_threads(channel, thread_id=thread_id)

    if error_check and thready is None:
        raise ValueError(f"Thread with id={thread_id} could not be found")

    return thready


@overload
async def please_give_me_the_channel(ctx: Union[ComponentContext, SlashContext, discord.Client],
                                     channel_id: int,
                                     error_check: Literal[True] = True,
                                     force_fetch: bool = False
                                     ) -> TextChannel:
    ...


@overload
async def please_give_me_the_channel(ctx: Union[ComponentContext, SlashContext, discord.Client],
                                     channel_id: int,
                                     error_check: Literal[False],
                                     force_fetch: bool = False,
                                     ) ->Optional[TextChannel]:
    ...



async def please_give_me_the_channel(ctx: Union[ComponentContext, SlashContext, discord.Client],
                                     channel_id: int,
                                     error_check: bool = True,
                                     force_fetch: bool = False
                                     ) -> Union[TextChannel, Optional[TextChannel]]:
    if isinstance(ctx, (ComponentContext, SlashContext)):
        channely = ctx.guild.get_channel(channel_id=channel_id)
        if force_fetch or channely is None:
            channely = await ctx.guild.fetch_channel(channel_id)
    elif isinstance(ctx, discord.Client):
        channely = ctx.get_channel(id=channel_id)
        if force_fetch or channely is None:
            channely = await ctx.fetch_channel(channel_id=channel_id)
    else:
        channely = None

    if error_check and channely is None:
        raise ValueError(f"channel with id={channel_id} could not be found")

    return channely  # type: ignore


class MatchProposalMessageHandler(MyCoggy):
    component_id = ComponentsIDs()

    # @timed_lru_cache(maxsize=314, seconds=60 * 5)
    def __init__(self):
        self.proposal_channel_lock_cache: CacheManager[int, asyncio.Lock] = CacheManager(safe_checks=False)

    def get_proposal_lock(self, channel_id: int) -> asyncio.Lock:
        if channel_id not in self.proposal_channel_lock_cache:
            self.proposal_channel_lock_cache[channel_id] = asyncio.Lock()
        return self.proposal_channel_lock_cache[channel_id]

    async def get_team_image(self, team_id: str) -> Optional[str]:
        async with aiohttp.ClientSession() as session:
            async with session.get(f"https://api.vrmasterleague.com/Teams/{team_id}") as resp:
                res = await resp.json()
                info = res['team']
                img = info.get('teamLogo', None)
                if img is None:
                    return None
                else:
                    return "https://vrmasterleague.com" + img

    async def create_components(self, is_confirmed: bool = False) -> list[dict]:
        buttons = [
            create_button(
                style=ButtonStyle.green,
                label="Yes, I can play",
                custom_id=self.component_id.yes_i_can_play_button(self)
            ),
            create_button(
                style=ButtonStyle.red,
                label="No, I can not play",
                custom_id=self.component_id.no_i_can_not_play_button(self)
            ),
            create_button(
                style=ButtonStyle.gray,
                label="I'm unsure",
                custom_id=self.component_id.im_unsure_button(self)
            ),
        ]

        if is_confirmed:
            confirm_style = ButtonStyle.red
            confirm_label = "Revoke match proposal"
        else:
            confirm_style = ButtonStyle.blue
            confirm_label = "Confirm match proposal"

        big_buttons = [
            create_button(
                style=confirm_style,
                label=confirm_label,
                custom_id=self.component_id.confirm_match_proposal_button(self)
            ),
        ]
        return [create_actionrow(*buttons), create_actionrow(*big_buttons)]

    async def create_components_proposal_message(self, is_archived: bool = False) -> list[dict]:
        if is_archived:
            confirm_style = ButtonStyle.blue
            confirm_label = "Unarchive match"
        else:
            confirm_style = ButtonStyle.red
            confirm_label = "Archive match"

        big_buttons = [
            create_button(
                style=confirm_style,
                label=confirm_label,
                custom_id=self.component_id.archive_a_match(self)
            ),
        ]
        return [create_actionrow(*big_buttons)]

    async def create_threaded_proposals(self, ctx: Union[ComponentContext, SlashContext],
                                        my_team: Team, my_registered_team: RegisteredTeam,
                                        enemy_team: Team, selected_times: list[datetime], match_duration: float,
                                        match_comment: Optional[str] = None):
        match_proposal_channel = await please_give_me_the_channel(
            ctx=ctx,
            channel_id=my_registered_team.discord_channel_proposals_id)

        if match_proposal_channel is None:
            raise SlashCommandException("Could not load the match proposal channel")

        if not isinstance(match_proposal_channel, TextChannel):
            raise SlashCommandException(f"The proposal channel should be a text channel, "
                                        f"instead got {type(match_proposal_channel)}")

        match_discussion_channel = await please_give_me_the_channel(
            ctx=ctx,
            channel_id=my_registered_team.discord_channel_discussion_id)

        if match_discussion_channel is None:
            raise SlashCommandException("Could not load the match discussion channel")

        if not isinstance(match_discussion_channel, TextChannel):
            raise SlashCommandException(f"The discussion channel should be a text channel, "
                                        f"instead got {type(match_discussion_channel)}")

        team_role: Role = ctx.guild.get_role(my_registered_team.discord_role_id)  # type: ignore

        discussion_thread_message = await match_discussion_channel.send(  # type: ignore
            content=f"the discussion for {team_role.mention} playing against {enemy_team.name}",
            components=await self.create_components_proposal_message()
        )

        discussion_thread = await match_discussion_channel.start_thread(name=enemy_team.name,
                                                                        message=discussion_thread_message)

        propose_thread_message = await match_proposal_channel.send(  # type: ignore
            content=f'{team_role.mention} playing against {enemy_team.name}',
            components=await self.create_components_proposal_message())
        propose_thread = await match_proposal_channel.start_thread(name=enemy_team.name, message=propose_thread_message)

        from blululu.tables import Match
        from blululu.tables import async_engine
        from sqlmodel.ext.asyncio.session import AsyncSession
        logger.debug(repr(selected_times))

        async with AsyncSession(async_engine, expire_on_commit=False) as session:
            match = Match(team_id=my_team.id, against_team_id=enemy_team.id, comment=match_comment)
            session.add(match)
            await session.merge(enemy_team)
            await session.commit()

            match_proposal_thread = MatchProposalThread(
                id=propose_thread.id,
                message_id=propose_thread_message.id,
                match_id=match.id)

            match_discussion_thread = MatchDiscussionThread(
                id=discussion_thread.id,
                message_id=discussion_thread_message.id,
                match_id=match.id)

            await self.update_match_all_thread_message(
                ctx=ctx,
                session=session,
                my_registered_team=my_registered_team,
                match_proposal_thread=match_proposal_thread,
                match_discussion_thread=match_discussion_thread,
                enemy_team=enemy_team,
                match=match
            )

            proposal_messages: list[tuple[Message, MatchProposal]] = []

            session.add(match_proposal_thread)
            session.add(match_discussion_thread)

            for selected_time in selected_times:
                mp = MatchProposal(match_id=match.id, is_confirmed=False, start_date=selected_time,
                                   end_date=selected_time + timedelta(hours=match_duration))
                propose_thread_message = await self.create_message(match_proposal_channel=propose_thread,
                                                                   match_proposal=mp)
                mp.message_id = propose_thread_message.id
                proposal_messages.append((propose_thread_message, mp))
                session.add(mp)
            await session.commit()

            playability_mps: dict[int, list[CanYouPlay]] = dict()

            for message, match_proposal in proposal_messages:
                timeslots: list[TimeSlot] = await session.run_sync(lambda _session: match_proposal.timeslots)
                timeslots = [ts for ts in timeslots if ts.match_id is None]
                playability = await self.convert_rows_to_playability(timeslots)
                match_proposal_votes = [
                    MatchProposalVotes(user_id=user_id, match_proposal_id=match_proposal.id, can_play=can_play)
                    for user_id, can_play in playability.items()
                ]
                for row in match_proposal_votes:
                    await session.merge(row)
                # updated_timeslots = await self.update_db_match_votes(
                #     date=match_proposal.date,
                #     duration=timedelta(hours=1),
                #     playability=playability)
                # logger.debug(f"{updated_timeslots=} \n {updated_match_proposal_votes=}")
                # for row in updated_timeslots:
                #     await session.merge(row)

                # rows_to_update = await self.update_db_match_votes(match_proposal=match_proposal, timeslots=timeslots)
                # for row in rows_to_update:
                #     await session.merge(row)
                for user_id, can_you_play_ in playability.items():
                    if user_id not in playability_mps:
                        playability_mps[user_id] = []
                    playability_mps[user_id].append(can_you_play_)
                await self.create_message_content(message, playability)
            await session.commit()

        for user_id, can_you_play_list in playability_mps.items():
            if (CanYouPlay.YES in can_you_play_list) or (CanYouPlay.MAYBE in can_you_play_list):
                # why am i doing this? because im not on a stable release of discord.py and the library is no
                # longer maintained because the one maintainer got butthurt that he wasnt involved in the
                # discord api new design. Anyway i just want threads and that is my only reason for using this
                # unstable thingy
                await discussion_thread.add_user(SnowFlaky(id=user_id))
            else:
                await discussion_thread.remove_user(SnowFlaky(id=user_id))

    async def update_match_all_thread_message(self,
                                              ctx: ComponentContext,
                                              session: sa_AsyncSession,
                                              my_registered_team: RegisteredTeam,
                                              match_proposal_thread: MatchProposalThread,
                                              match_discussion_thread: MatchDiscussionThread,
                                              enemy_team: Team,
                                              match: Match
                                              ):
        confirmed_match_proposal: Optional[MatchProposal] = None
        match_proposals: list[MatchProposal] = await session.run_sync(lambda _s: match.match_proposals)
        for mp in match_proposals:
            if mp.is_confirmed and confirmed_match_proposal is None:
                confirmed_match_proposal = mp
            elif mp.is_confirmed and confirmed_match_proposal is not None:
                raise ValueError("reaching this line means there are more than one confirmed proposal. "
                                 "which is absurd, please review what happened that lead to this point")

        proposal_channel = await please_give_me_the_channel(
            ctx=ctx,
            channel_id=my_registered_team.discord_channel_proposals_id
        )
        if proposal_channel is None:
            raise ValueError("proposal_channel should not be None")
        propose_thread_message = PartialMessage(channel=proposal_channel,
                                                id=match_proposal_thread.message_id)
        propose_thread = await please_give_me_the_thread(
            ctx=ctx,
            thread_id=match_proposal_thread.id,
            channel=proposal_channel
        )

        discussion_channel = await please_give_me_the_channel(
            ctx=ctx,
            channel_id=my_registered_team.discord_channel_discussion_id
        )

        discussion_thread_message = PartialMessage(channel=discussion_channel,
                                                   id=match_discussion_thread.message_id)

        discussion_thread = await please_give_me_the_thread(
            ctx=ctx,
            thread_id=match_discussion_thread.id,
            channel=discussion_channel
        )

        await propose_thread_message.edit(embed=await self.get_match_proposal_embed(
            enemy_team=enemy_team,
            comment=match.comment,
            proposal_thread=propose_thread,
            discussion_thread=discussion_thread,
            confirmed_match_proposal=confirmed_match_proposal
        ))

        await discussion_thread_message.edit(embed=await self.get_match_discussion_embed(
            enemy_team=enemy_team,
            comment=match.comment,
            proposal_thread=propose_thread,
            confirmed_match_proposal=confirmed_match_proposal
        ))

    async def create_match_embed(self, enemy_team: Team, extra_desc: Optional[dict[str, str]] = None) -> discord.Embed:
        proposal_embed = discord.Embed()
        proposal_embed.title = enemy_team.name
        enemy_team_logo_url = await self.get_team_image(enemy_team.id)
        if enemy_team_logo_url is not None:
            proposal_embed.set_image(url=enemy_team_logo_url)

        desc: dict[str, str] = dict()
        if extra_desc is not None:
            desc.update(**extra_desc)
        desc['team page'] = \
            f"[{enemy_team.name}](https://vrmasterleague.com/{enemy_team.game.value}/Teams/{enemy_team.id})"

        desc_text = '\n'.join(f'{key} = {value}' for key, value in desc.items())

        proposal_embed.description = desc_text

        return proposal_embed

    async def get_match_proposal_embed(self, enemy_team: Team, comment: Optional[str],
                                       discussion_thread: Thread,
                                       proposal_thread: Thread,
                                       confirmed_match_proposal: Optional[MatchProposal] = None) -> discord.Embed:
        extra_desc = dict()
        if comment is not None:
            extra_desc['comment'] = comment

        if confirmed_match_proposal is not None:
            partial_message = ThreadPartialMessage(channel=proposal_thread, id=confirmed_match_proposal.message_id)
            extra_desc['confirmed'] = f"[YES]({partial_message.jump_url})"
            timestampy = int(confirmed_match_proposal.start_date.timestamp())
            extra_desc['time'] = f'<t:{timestampy}:F> (<t:{timestampy}:R>)'
        else:
            extra_desc['confirmed'] = 'NO'

        extra_desc['discussion thread'] = f"<#{discussion_thread.id}>"

        return await self.create_match_embed(enemy_team=enemy_team, extra_desc=extra_desc)

    async def get_match_discussion_embed(self, enemy_team: Team, comment: Optional[str], proposal_thread: Thread,
                                         confirmed_match_proposal: Optional[MatchProposal] = None) -> discord.Embed:
        extra_desc = dict()
        if comment is not None:
            extra_desc['comment'] = comment

        if confirmed_match_proposal is not None:
            partial_message = ThreadPartialMessage(channel=proposal_thread, id=confirmed_match_proposal.message_id)
            extra_desc['confirmed'] = f"[YES]({partial_message.jump_url})"
            f"[YES]({confirmed_match_proposal.message_id})"
        else:
            extra_desc['confirmed'] = 'NO'

        extra_desc['proposal thread'] = f"<#{proposal_thread.id}>"

        return await self.create_match_embed(enemy_team=enemy_team, extra_desc=extra_desc)

    @classmethod
    async def update_db_match_votes(cls, match_proposal: MatchProposal, timeslots: list[TimeSlot],
                                    can_play: CanYouPlay) \
            -> list[Union[TimeSlot, MatchProposalVotes]]:
        # playability = await cls.convert_rows_to_playability(timeslots)
        rows: list[Union[TimeSlot, MatchProposalVotes]] = []
        timeslots_group: dict[int, list[TimeSlot]] = dict()
        match_length = match_proposal.end_date - match_proposal.start_date
        datetime_continues = set(match_proposal.start_date + timedelta(seconds=i)
                                 for i in range(0, match_length.seconds, 60 * 30))
        for t in timeslots:
            if t.user_id not in timeslots_group:
                timeslots_group[t.user_id] = []

            old_can_play = t.can_play
            new_can_lay = can_play
            if old_can_play is CanYouPlay.MAYBE:
                res_can_play = new_can_lay
            elif old_can_play is not new_can_lay:
                res_can_play = CanYouPlay.MAYBE
            elif old_can_play is new_can_lay:
                res_can_play = old_can_play
            else:
                raise ValueError("greetings young traveler, you have reached an unexpected point. evaluate your life"
                                 " choices that lead you to reach this line")

            t.can_play = res_can_play
            timeslots_group[t.user_id].append(t)

        for user_id, timeslots_user in timeslots_group.items():
            user_dates = set(t.date for t in timeslots_user)
            for dt in (datetime_continues - user_dates):
                timeslots_user.append(TimeSlot(user_id=user_id, date=dt, can_play=can_play))

            rows.extend(timeslots_user)

        return rows

    # this function will bite me in the ass later. NOTE: TimeSlot might conflict with MatchProposal in the result
    # so this TypeVar is created to type hint the fact that the function either takes a list of full MatchProposal
    # or a list full of TimeSlot but not both.
    @staticmethod
    async def convert_rows_to_playability(rows: Union[list[TimeSlot], list[MatchProposalVotes]]) \
            -> dict[int, CanYouPlay]:
        group: dict[int, list[CanYouPlay]] = dict()
        for row in rows:
            if row.user_id not in group:
                group[row.user_id] = []
            group[row.user_id].append(row.can_play)

        res: dict[int, CanYouPlay] = dict()
        for user_id, group_rows in group.items():
            yes = CanYouPlay.YES in group_rows
            no = CanYouPlay.NO in group_rows
            maybe = CanYouPlay.MAYBE in group_rows

            if maybe or (yes and no):
                res[user_id] = CanYouPlay.MAYBE
            elif yes:
                res[user_id] = CanYouPlay.YES
            elif no:
                res[user_id] = CanYouPlay.NO
            elif all(cyp is None for cyp in group_rows):
                # here if the can_play column has some nones in it
                res[user_id] = CanYouPlay.NO
            else:
                raise ValueError("good job, you have defied logic, read the stacks and the logs for why we reached "
                                 f"this line. here is the {group_rows=} for {user_id=}")

        return res

    @staticmethod
    async def create_message_content(message: Union[Message, PartialMessage], playability: dict[int, CanYouPlay]):
        users_playability: dict[CanYouPlay, list[int]] = {
            CanYouPlay.YES: [],
            CanYouPlay.NO: [],
            CanYouPlay.MAYBE: [],
        }
        labels = {
            CanYouPlay.YES: "Yes, i can play",
            CanYouPlay.NO: "No, I cannot play",
            CanYouPlay.MAYBE: "Maybe",
        }

        for user, can_you_play in playability.items():
            users_playability[can_you_play].append(user)

        content_lines = []
        for can_you_play, label in labels.items():
            users_mentions = ','.join([f'<@{u}>' for u in users_playability[can_you_play]])
            content_lines.append(f"{label}: {users_mentions}")
        content = '\n'.join(content_lines)
        await message.edit(content=content)

    async def create_match_proposal_embed(self, match_proposal: MatchProposal) -> discord.Embed:
        embed = discord.Embed()
        start_time = match_proposal.start_date
        end_time = match_proposal.end_date
        start_timestamp = int(start_time.astimezone(timezone.utc).timestamp())
        end_timestamp = int(end_time.astimezone(timezone.utc).timestamp())
        embed.description = f"<t:{start_timestamp}:f> (<t:{start_timestamp}:R>)\nto\n" \
                            f"<t:{end_timestamp}:f> (<t:{end_timestamp}:R>)"
        return embed

    async def create_message(self, match_proposal_channel: Thread, match_proposal: MatchProposal) -> Message:
        embed = await self.create_match_proposal_embed(match_proposal=match_proposal)
        return await match_proposal_channel.send(embed=embed, components=await self.create_components())  # type: ignore

    async def update_can_play(self, ctx: ComponentContext, can_play: CanYouPlay):
        from blululu.tables import async_engine
        async with AsyncSession(async_engine, expire_on_commit=False) as session:
            await self.better_update_can_play(session=session, can_play=can_play, origin_message=ctx.origin_message,
                                              ctx=ctx,
                                              author_id=ctx.author_id,
                                              skip_conflict_checks=False)
        await ctx.defer(ignore=True)

    @classmethod
    async def better_update_can_play(cls, session: AsyncSession, can_play: CanYouPlay,
                                     origin_message: Union[Message, PartialMessage],
                                     author_id: int,
                                     ctx: ComponentContext,
                                     update_timeslots: bool = True,
                                     skip_conflict_checks: bool = True,
                                     ) -> bool:
        from blululu.tables import MatchProposal, MatchProposalVotes, Match
        from sqlmodel import select

        statement = select(MatchProposal).where(MatchProposal.message_id == origin_message.id)
        match_proposal: MatchProposal = (await session.exec(statement)).one()  # type: ignore

        timeslots: list[TimeSlot] = await session.run_sync(lambda _session: match_proposal.timeslots)
        match: Match = await session.run_sync(lambda _s: match_proposal.match)
        logger.debug(f"im getting voted, {can_play=}")
        if not skip_conflict_checks and can_play is CanYouPlay.YES:
            res = await cls.conflicted_confirmed_matches(
                current_match_proposal=match_proposal,
                current_match=match,
                session=session,
                current_users_votes={author_id: CanYouPlay.YES}
            )
            if len(res[CanYouPlay.YES]) > 0:
                links = cls.create_conflicted_message(group=res[CanYouPlay.YES], include_user=False)
                message = f'You cant attend this match since you have another conflicting confirmed match.\n{links}\n'
                raise SlashCommandException(message=message, send_new=True, hidden=True)

        match_proposal_votes: list[MatchProposalVotes] = await session.run_sync(
            lambda _session: match_proposal.match_proposal_votes)
        has_changed = True
        for match_proposal_vote in match_proposal_votes:
            if match_proposal_vote.user_id == author_id:
                has_changed = match_proposal_vote.can_play is not can_play
                match_proposal_vote.can_play = can_play
                break
        else:
            match_proposal_votes.append(MatchProposalVotes(user_id=author_id,
                                                           match_proposal_id=match_proposal.id,
                                                           can_play=can_play))

        for row in match_proposal_votes:
            await session.merge(row)

        if update_timeslots:
            rows_to_update = await cls.update_db_match_votes(match_proposal=match_proposal, timeslots=timeslots,
                                                             can_play=can_play)
            for row_to_update in rows_to_update:
                await session.merge(row_to_update)

        await session.commit()
        playability = await cls.convert_rows_to_playability(match_proposal_votes)
        await cls.create_message_content(origin_message, playability)
        playability_mps: list[CanYouPlay] = []

        registered_team: RegisteredTeam = await session.run_sync(
            lambda _s: match.proposerer_team
        )
        match_discussion_thread: MatchDiscussionThread = await session.run_sync(
            lambda _s: match.match_discussion_thread)
        match_all_proposal_votes: list[MatchProposalVotes] = await session.run_sync(
            lambda _s: match.match_proposal_votes)
        for mpvs in match_all_proposal_votes:
            if mpvs.user_id == author_id:
                playability_mps.append(mpvs.can_play)

        thread = await please_give_me_the_thread(
            ctx=ctx,
            thread_id=match_discussion_thread.id,
            channel=await please_give_me_the_channel(
                ctx=ctx,
                channel_id=registered_team.discord_channel_discussion_id
            )
        )
        if (CanYouPlay.YES in playability_mps) or (CanYouPlay.MAYBE in playability_mps):
            await thread.add_user(SnowFlaky(id=author_id))
        else:
            await thread.remove_user(SnowFlaky(id=author_id))

        logger.debug(f"{match_all_proposal_votes=}")
        return has_changed

    @MyCoggy.component_callback(components=component_id.yes_i_can_play_button)
    async def yes_i_can_play_button(self, ctx: ComponentContext):
        await self.update_can_play(ctx=ctx, can_play=CanYouPlay.YES)

    @MyCoggy.component_callback(components=component_id.no_i_can_not_play_button)
    async def no_i_can_not_play_button(self, ctx: ComponentContext):
        await self.update_can_play(ctx=ctx, can_play=CanYouPlay.NO)

    @MyCoggy.component_callback(components=component_id.im_unsure_button)
    async def im_unsure_button(self, ctx: ComponentContext):
        await self.update_can_play(ctx=ctx, can_play=CanYouPlay.MAYBE)

    @staticmethod
    async def conflicted_confirmed_matches(current_match_proposal: MatchProposal,
                                           current_match: Match,
                                           session: AsyncSession,
                                           current_users_votes: Optional[dict[int, CanYouPlay]] = None
                                           ) \
            -> dict[CanYouPlay, dict[int, list[tuple[RegisteredTeam, MatchProposalThread, MatchProposal]]]]:
        from sqlmodel import select, not_
        users = await session.run_sync(lambda _s: current_match_proposal.match_proposal_votes)

        if current_users_votes is None:
            current_users_votes = dict()
            for u in users:
                if u.can_play is CanYouPlay.YES or u.can_play is CanYouPlay.MAYBE:
                    current_users_votes[u.user_id] = u.can_play

        # assert current_users_votes is not None
        user_ids = list(current_users_votes.keys())
        match_time_intersection = not_((MatchProposal.end_date <= current_match_proposal.start_date) |
                                       (MatchProposal.start_date >= current_match_proposal.end_date))

        statement = select(RegisteredTeam, MatchProposalThread, MatchProposal, MatchProposalVotes
                           ).join(MatchProposal
                                ).join(Match
                                    ).join(MatchProposalThread
                                        ).join(RegisteredTeam, RegisteredTeam.id == Match.team_id).where(
            (MatchProposalVotes.can_play == CanYouPlay.YES) | (MatchProposalVotes.can_play == CanYouPlay.MAYBE),
            MatchProposalVotes.user_id.in_(user_ids),  # type: ignore
            MatchProposal.match_id != current_match.id,
            MatchProposal.is_confirmed == true(),
            match_time_intersection,
        )

        res = (await session.exec(statement)).all()  # type: ignore

        mpv_groups2: dict[CanYouPlay, dict[int, list[tuple[RegisteredTeam, MatchProposalThread, MatchProposal]]]] \
            = dict()
        mpv_groups2[CanYouPlay.YES] = dict()
        mpv_groups2[CanYouPlay.MAYBE] = dict()
        for rt, mpt, mp, mpv in res:
            can_play = mpv.can_play #current_users_votes[mpv.user_id]
            if mpv.user_id not in mpv_groups2[can_play]:
                mpv_groups2[can_play][mpv.user_id] = []
            o = (rt, mpt, mp)
            if o not in mpv_groups2[can_play][mpv.user_id]:
                mpv_groups2[can_play][mpv.user_id].append(o)


        return mpv_groups2

    @staticmethod
    def create_conflicted_message(group: dict[int, list[tuple[RegisteredTeam, MatchProposalThread, MatchProposal]]],
                                  include_user: bool = True,
                                  ) -> str:
        conflicts: list[str] = []
        for user_id, conflicts_data in group.items():
            links: list[str] = []
            for rt, mpt, mp in conflicts_data:
                links.append(f'\t\t- https://discord.com/channels/{rt.discord_guild_id}/{mpt.id}/{mp.message_id}')
            m = '\n'.join(links)
            if include_user:
                m = f'\t <@{user_id}>\n{m}'

            conflicts.append(m)

        return '\n'.join(conflicts)

    async def check_conflicted_matches(self,
                                       current_match_proposal: MatchProposal,
                                       current_match: Match,
                                       session: AsyncSession
                                       ) -> str:
        shared_timeslots = await self.conflicted_confirmed_matches(
            current_match_proposal=current_match_proposal,
            current_match=current_match,
            session=session
        )

        warning_message = ''
        logger.debug(f"{shared_timeslots[CanYouPlay.MAYBE]=}")
        logger.debug(f"{shared_timeslots[CanYouPlay.YES]=}")
        if len(shared_timeslots[CanYouPlay.MAYBE])>0:
            warning_message += 'WARNING: The following players have voted "I\'m unsure" on other confirmed matches. ' \
                               'This might result in a conflict between the matches if the player attended the match. ' \
                               'Copy the current message and let the mentioned players know to resolve their ' \
                               'conflicts. \n'
            warning_message += self.create_conflicted_message(group=shared_timeslots[CanYouPlay.MAYBE])

        if len(shared_timeslots[CanYouPlay.YES]) > 0:
            message = 'The following players have conflicted confirmed matches. Therefore, this match proposal cannot ' \
                      'be confirmed. Once the conflicts has been resolved the proposal can be confirmed. ' \
                      'Copy the current message and let the mentioned players know to resolve their conflicts. \n'
            message += self.create_conflicted_message(group=shared_timeslots[CanYouPlay.YES])
            message += f"\n\n{warning_message}\n"
            raise SlashCommandException(message=message)

        return warning_message

    @MyCoggy.component_callback(components=component_id.confirm_match_proposal_button)
    async def confirm_match_proposal_button(self, ctx: ComponentContext):
        await ctx.defer(hidden=True)
        async with self.get_proposal_lock(channel_id=ctx.channel_id):
            await self._confirm_match_proposal_button(ctx=ctx)

    async def _confirm_match_proposal_button(self, ctx: ComponentContext):
        from blululu.tables import MatchProposal, async_engine, User
        from sqlmodel import select

        async with AsyncSession(async_engine, expire_on_commit=False) as session:
            statement = select(MatchProposal).where(MatchProposal.message_id == ctx.origin_message_id)
            match_proposal: MatchProposal = (await session.exec(statement)).one()  # type: ignore
            match: Match = await session.run_sync(lambda _s: match_proposal.match)
            my_registered_team: RegisteredTeam = await session.run_sync(lambda _s: match.proposerer_team)
            match_proposal_thread: MatchProposalThread = await session.run_sync(lambda _s: match.match_proposal_thread)
            match_discussion_thread: MatchDiscussionThread = await session.run_sync(
                lambda _s: match.match_discussion_thread)
            enemy_team: Team = await session.run_sync(lambda _s: match.against_team)

            logger.debug(f"{match_proposal=}")
            logger.debug(f"{enemy_team=}")
            if match_proposal.is_confirmed:
                confirming_match = False  # no judding, i know i can do something like
                # `confirming_match = not match_proposal.is_confirmed` but that is less readable with the if statement

                await self.revoke_a_match_proposal(ctx=ctx, session=session,
                                                   match_proposal=match_proposal,
                                                   match_proposal_thread=match_proposal_thread,
                                                   my_registered_team=my_registered_team)
            else:
                confirming_match = True
                await self.actually_confirms_the_match(ctx=ctx, session=session,
                                                       match=match,
                                                       match_proposal=match_proposal,
                                                       match_proposal_thread=match_proposal_thread,
                                                       my_registered_team=my_registered_team,
                                                       match_discussion_thread=match_discussion_thread)

            await session.commit()

            await self.update_match_all_thread_message(
                ctx=ctx,
                session=session,
                my_registered_team=my_registered_team,
                match_proposal_thread=match_proposal_thread,
                match_discussion_thread=match_discussion_thread,
                enemy_team=enemy_team,
                match=match
            )

        # await ctx.defer(ignore=True)
        if confirming_match:
            await ctx.send("Match proposal is confirmed!", hidden=True)
        else:
            await ctx.send("Match proposal is revoked", hidden=True)

    async def revoke_a_match_proposal(self, ctx: ComponentContext,
                                      session: AsyncSession,
                                      match_proposal: MatchProposal,
                                      match_proposal_thread: MatchProposalThread,
                                      my_registered_team: RegisteredTeam
                                      ) -> ThreadPartialMessage:

        old_timeslots: list[TimeSlot] = await session.run_sync(lambda _s: match_proposal.timeslots)
        for old_ts in old_timeslots:
            # yes i want to null this
            old_ts.match_id = None  # type: ignore
            await session.merge(old_ts)

        match_proposal.is_confirmed = False
        await session.merge(match_proposal)
        thread_proposals = await please_give_me_the_thread(
            ctx=ctx,
            thread_id=match_proposal_thread.id,
            channel=await please_give_me_the_channel(
                ctx=ctx,
                channel_id=my_registered_team.discord_channel_proposals_id
            )
        )
        message = ThreadPartialMessage(channel=thread_proposals, id=match_proposal.message_id)
        await message.edit(components=await self.create_components(is_confirmed=False))
        return message

    async def confirm_a_match_proposal(self,
                                       ctx: ComponentContext,
                                       session: AsyncSession, match_proposal: MatchProposal,
                                       match_proposal_thread: MatchProposalThread,
                                       my_registered_team: RegisteredTeam
                                       ) -> ThreadPartialMessage:

        match_proposal.is_confirmed = True
        await session.merge(match_proposal)
        new_timeslots: list[TimeSlot] = await session.run_sync(lambda _s: match_proposal.timeslots)
        for new_ts in new_timeslots:
            new_ts.match_id = match_proposal.match_id
            await session.merge(new_ts)

        thread_proposals = await please_give_me_the_thread(
            ctx=ctx,
            thread_id=match_proposal_thread.id,
            channel=await please_give_me_the_channel(
                ctx=ctx,
                channel_id=my_registered_team.discord_channel_proposals_id
            )
        )
        message = ThreadPartialMessage(channel=thread_proposals, id=match_proposal.message_id)
        await message.edit(components=await self.create_components(is_confirmed=True))
        return message

    async def actually_confirms_the_match(self, ctx: ComponentContext,
                                          session: AsyncSession,
                                          match: Match,
                                          match_proposal: MatchProposal,
                                          match_proposal_thread: MatchProposalThread,
                                          my_registered_team: RegisteredTeam,
                                          match_discussion_thread: MatchDiscussionThread):
        from sqlmodel import select, true

        warning_message = await self.check_conflicted_matches(
            current_match_proposal=match_proposal,
            current_match=match,
            session=session
        )

        statement = select(MatchProposal).where(MatchProposal.match_id == match_proposal.match_id,
                                                MatchProposal.is_confirmed == true())
        confirmed_match_proposal: list[MatchProposal] = (await session.exec(statement)).all()  # type: ignore

        for old_cmp in confirmed_match_proposal:
            if old_cmp.is_confirmed:
                await self.revoke_a_match_proposal(ctx=ctx, session=session, match_proposal=old_cmp,
                                                   match_proposal_thread=match_proposal_thread,
                                                   my_registered_team=my_registered_team)

        match_proposal_message = await self.confirm_a_match_proposal(
            ctx=ctx,
            session=session,
            match_proposal=match_proposal,
            match_proposal_thread=match_proposal_thread,
            my_registered_team=my_registered_team
        )
        discussion_thread = await please_give_me_the_thread(
            ctx=ctx,
            thread_id=match_discussion_thread.id,
            channel=await please_give_me_the_channel(
                ctx=ctx,
                channel_id=my_registered_team.discord_channel_discussion_id
            )
        )

        embed = await self.create_match_proposal_embed(match_proposal=match_proposal)
        embed.description = f'[Match Proposal]({match_proposal_message.jump_url})\n' + embed.description
        await discussion_thread.send(content=f"{ctx.author.mention} has confirmed this proposal\n{warning_message}",
                                     embed=embed)

    async def archive_match(self,
                            ctx: Union[ComponentContext, SlashContext],
                            session: AsyncSession,
                            match_proposal_thread: Optional[MatchProposalThread],
                            match_discussion_thread: Optional[MatchDiscussionThread],
                            archive: Optional[bool] = None
                            ) -> None:
        match: Match
        match_proposal_thread_: MatchProposalThread
        match_discussion_thread_: MatchDiscussionThread
        if match_proposal_thread is not None:
            match_proposal_thread_ = match_proposal_thread
            match = await session.run_sync(lambda _s: match_proposal_thread_.match)
            match_discussion_thread_ = await session.run_sync(
                lambda _s: match.match_discussion_thread)
            is_propose = True

        elif match_discussion_thread is not None:
            match_discussion_thread_ = match_discussion_thread
            match = await session.run_sync(lambda _s: match_discussion_thread_.match)
            match_proposal_thread_ = await session.run_sync(
                lambda _s: match.match_proposal_thread)
            is_propose = False

        else:
            raise ValueError(f"we shouldn't even reach this line. "
                             f"{match_discussion_thread=} "
                             f"{match_proposal_thread=}")
        registered_team: RegisteredTeam = await session.run_sync(lambda _s: match.proposerer_team)
        discussion_channel = await please_give_me_the_channel(
            ctx=ctx,
            channel_id=registered_team.discord_channel_discussion_id)
        proposal_channel = await please_give_me_the_channel(
            ctx=ctx,
            channel_id=registered_team.discord_channel_proposals_id
        )
        if is_propose:
            non_ctx_message = discussion_channel.get_partial_message(match_discussion_thread_.message_id)
        else:
            non_ctx_message = proposal_channel.get_partial_message(match_proposal_thread_.message_id)
        match_proposal_thread_ctx = await please_give_me_the_thread(ctx=ctx,
                                                                    thread_id=match_proposal_thread_.id,
                                                                    channel=proposal_channel,
                                                                    error_check=False)
        match_discussion_thread_ctx = await please_give_me_the_thread(ctx=ctx,
                                                                      thread_id=match_discussion_thread_.id,
                                                                      channel=discussion_channel,
                                                                      error_check=False)
        if match_proposal_thread_ctx is None or match_discussion_thread_ctx is None:
            raise SlashCommandException(message="Sorry tho, if some of the threads are archived, try to unarchived "
                                                "them and try again",
                                        send_new=True,
                                        hidden=True)
        my_registered_team: RegisteredTeam = await session.run_sync(
            lambda _s: match.proposerer_team
        )
        enemy_team: Team = await session.run_sync(
            lambda _s: match.against_team
        )
        # the lines bellow is to make sure the state of the threads matches the state of the database
        if match_proposal_thread_ctx.archived != match.is_archived:
            await match_proposal_thread_ctx.edit(archived=match.is_archived)
        if match_discussion_thread_ctx.archived != match.is_archived:
            await match_discussion_thread_ctx.edit(archived=match.is_archived)

        if archive is None:
            # toggle mode
            unarchiving = match.is_archived
        else:
            # manual mode
            unarchiving = not archive

        if unarchiving:
            match.is_archived = False
            await match_discussion_thread_ctx.send(content=f"{ctx.author.mention} has unarchived this match")
        else:
            match_proposals: list[MatchProposal] = await session.run_sync(
                lambda _s: match.match_proposals
            )
            for mp in match_proposals:
                if mp.is_confirmed:
                    await self.revoke_a_match_proposal(
                        ctx=ctx,
                        session=session,
                        match_proposal=mp,
                        match_proposal_thread=match_proposal_thread_,
                        my_registered_team=my_registered_team
                    )

            match.is_archived = True
            await match_discussion_thread_ctx.send(content=f"{ctx.author.mention} has archived this match")
        await match_proposal_thread_ctx.edit(archived=match.is_archived)
        await match_discussion_thread_ctx.edit(archived=match.is_archived)
        await ctx.edit_origin(
            components=await self.create_components_proposal_message(is_archived=match.is_archived))
        await non_ctx_message.edit(
            components=await self.create_components_proposal_message(is_archived=match.is_archived))
        await session.merge(match)
        await session.commit()
        logger.debug(f'{unarchiving=}')
        if not unarchiving:
            await self.update_match_all_thread_message(
                ctx=ctx,
                session=session,
                my_registered_team=my_registered_team,
                match_proposal_thread=match_proposal_thread_,
                match_discussion_thread=match_discussion_thread_,
                enemy_team=enemy_team,
                match=match
            )

    @MyCoggy.component_callback(components=component_id.archive_a_match)
    async def archive_a_match_button(self, ctx: ComponentContext):
        from blululu.tables import MatchProposalThread, async_engine, MatchDiscussionThread
        from sqlmodel import select
        async with AsyncSession(async_engine, expire_on_commit=False) as session:
            statement_proposal = select(MatchProposalThread).where(
                MatchProposalThread.message_id == ctx.origin_message_id)
            statement_discussion = select(MatchDiscussionThread).where(
                MatchDiscussionThread.message_id == ctx.origin_message_id)
            match_proposal_threads: list[MatchProposalThread] = (
                await session.exec(statement_proposal)).all()  # type: ignore
            match_discussion_threads: list[MatchDiscussionThread] = (
                await session.exec(statement_discussion)).all()  # type: ignore
            if len(match_proposal_threads) == 1:
                match_proposal_thread = match_proposal_threads[0]
                match_discussion_thread = None
            elif len(match_discussion_threads) == 1:
                match_proposal_thread = None
                match_discussion_thread = match_discussion_threads[0]

            else:
                raise ValueError(f"we shouldn't even reach this line. "
                                 f"{match_discussion_threads=} "
                                 f"{match_proposal_threads=}")

            await self.archive_match(
                ctx=ctx,
                match_proposal_thread=match_proposal_thread,
                match_discussion_thread=match_discussion_thread,
                session=session,
                archive=None
            )

    # what a disappointing thing, discord-slash/discord.py somehow does not load a thread if i specified
    # SlashCommandOptionType.CHANNEL, this is sad but will leave it later
    # @MyCoggy.slash(name='archive-match',
    #                description='to archive a match via the thread name',
    #                guild_ids=debug_guilds,
    #                options=[
    #                    create_option(name='thread',
    #                                  description='match thread to archive',
    #                                  option_type=SlashCommandOptionType.CHANNEL,
    #                                  required=True
    #                                  ),
    #                    create_option(name='action', description='archive or unarchive (default is toggle mode)',
    #                                  option_type=SlashCommandOptionType.STRING,
    #                                  required=False,
    #                                  choices=['archive', 'unarchive'])
    #                ]
    #                )
    # async def archive_a_match_command(self, ctx: SlashContext,
    #                                   thread: Union[GuildChannel, Thread],
    #                                   action: Optional[Literal['archive', 'unarchive']] = None
    #                                   ):
    #     logger.debug(f"{thread=}")
    #     if not isinstance(thread, Thread):
    #         raise SlashCommandException(message='please provide a valid thread')
    #
    #     from botty.tables import MatchProposalThread, async_engine, MatchDiscussionThread
    #     from sqlmodel import select
    #     async with AsyncSession(async_engine, expire_on_commit=False) as session:
    #         statement_proposal = select(MatchProposalThread).where(
    #             MatchProposalThread.id == thread.id)
    #         statement_discussion = select(MatchDiscussionThread).where(
    #             MatchDiscussionThread.id == thread.id)
    #
    #         match_proposal_threads: list[MatchProposalThread] = (await session.exec(statement_proposal)).all()
    #         match_discussion_threads: list[MatchDiscussionThread] = (await session.exec(statement_discussion)).all()
    #         if len(match_proposal_threads) == 1:
    #             match_proposal_thread = match_proposal_threads[0]
    #             match_discussion_thread = None
    #         elif len(match_discussion_threads) == 1:
    #             match_proposal_thread = None
    #             match_discussion_thread = match_discussion_threads[0]
    #         else:
    #             raise ValueError(f"we shouldn't even reach this line. "
    #                              f"{match_discussion_threads=} "
    #                              f"{match_proposal_threads=}")
    #
    #         if action=='archive':
    #             archive = True
    #         elif action == 'unarchive':
    #             archive = False
    #         else:
    #             archive = None
    #
    #         await self.archive_match(
    #             ctx=ctx,
    #             match_proposal_thread=match_proposal_thread,
    #             match_discussion_thread=match_discussion_thread,
    #             session=session,
    #             archive=archive
    #         )

    async def is_thread_about_to_archive(self,
                                         thread: Thread,
                                         sanity_offset: timedelta = timedelta(minutes=30)) -> Optional[bool]:
        ard = timedelta(minutes=thread.auto_archive_duration)
        last_message = thread.last_message
        try:
            if last_message is None:
                last_message = await thread.fetch_message(thread.last_message_id)
            created_at = last_message.created_at
        except discord.errors.NotFound as e:
            # logger.debug(f"{e=}")
            # if the message could not be found assume the last message was just sent near the archiving period
            created_at = datetime.now().astimezone(tz=timezone.utc) - ard
        last_message_created_at = created_at.replace(tzinfo=timezone.utc)
        archive_point = last_message_created_at + ard
        current_time = datetime.now().astimezone(tz=timezone.utc)
        # logger.debug(f"===\n{last_message_created_at=}\n{archive_point=}\n{current_time=}\n{sanity_offset=}\n===")
        return archive_point <= (current_time + sanity_offset)

    async def send_heartbeat_message(self, thread: Thread) -> Message:
        async def send_func(
                content: str,
                embed: discord.Embed = None,
                components: list[dict] = None,
        ) -> Union[dict, discord.Message]:
            return await thread.send(content=content, embed=embed, components=components)  # type: ignore

        prefix = "this is a heartbeat message to keep the thread alive to not auto archive. " \
                 "In the meantime you can check the help messages down here."
        return await MyCoggy.send_random_help_message(send_func=send_func,
                                                      channel_id=thread.id,
                                                      prefix=prefix)

    async def process_thread_heartbeating(self,
                                          match_proposal_thread: Union[MatchProposalThread, MatchDiscussionThread],
                                          match_proposal_thread_ctx: Thread,
                                          sanity_offset: timedelta,
                                          force_fetch: bool = False):

        if match_proposal_thread_ctx is not None:
            logger.debug(f"{match_proposal_thread_ctx=}")
            if match_proposal_thread_ctx.archived:
                await match_proposal_thread_ctx.edit(archived=False)
                bombadombakomba = False  # idk if i got angry i might turn this on.
                if bombadombakomba:
                    await match_proposal_thread_ctx.send(
                        content="listing here you little shits, there are TWO FUCKING BUTTONS for archiving the match. "
                                "so you must have seen them by now. please use them instead of archiving MY threads "
                                "directly.")
            if await self.is_thread_about_to_archive(match_proposal_thread_ctx):
                if match_proposal_thread.last_heartbeat_message_id is not None:
                    logger.debug(f"{match_proposal_thread.last_heartbeat_message_id=}")
                    if force_fetch:
                        for msg in match_proposal_thread_ctx.history(limit=25, oldest_first=False):  # type: ignore
                            if msg.id == match_proposal_thread.last_heartbeat_message_id:
                                await msg.delete()
                                break
                        else:
                            logger.error("could not find the message with force fetch in the thread heartbeeater")
                    else:
                        try:
                            await match_proposal_thread_ctx.delete_messages([
                                ThreadPartialMessage(channel=match_proposal_thread_ctx,
                                                     id=match_proposal_thread.last_heartbeat_message_id)
                            ])
                        except discord.errors.NotFound:
                            logger.error(msg="could not find the thread message", exc_info=sys.exc_info())
                            if not force_fetch:
                                raise
                            else:
                                pass

                heartbeat_message = await self.send_heartbeat_message(thread=match_proposal_thread_ctx)

                match_proposal_thread.last_heartbeat_message_id = heartbeat_message.id

    thread_heartbeating = 60 * 50

    @MyCoggy.loop(seconds=thread_heartbeating)
    async def thread_heartbeater(self):
        while not self.bot.is_ready():
            await asyncio.sleep(5)

        from blululu.tables import MatchProposalThread, async_engine, Match, MatchDiscussionThread
        from sqlmodel import select

        async with AsyncSession(async_engine) as session:
            statement = select(Match).where(Match.is_archived == false())
            matches: list[Match] = (await session.exec(statement)).all()
            for match in matches:
                match_proposal_thread: MatchProposalThread = await session.run_sync(
                    lambda _s: match.match_proposal_thread)
                match_discussion_thread: MatchDiscussionThread = await session.run_sync(
                    lambda _s: match.match_discussion_thread)
                registered_team: RegisteredTeam = await session.run_sync(
                    lambda _s: match.proposerer_team
                )

                if match_proposal_thread is None or match_discussion_thread is None:  # database glitchy bitchy
                    continue

                # TODO: make a superclass of these two classes and use it here instead
                match_thread_channel_ids: list[tuple[Union[MatchProposalThread, MatchDiscussionThread], int]] = [
                    (match_proposal_thread, registered_team.discord_channel_proposals_id),
                    (match_discussion_thread, registered_team.discord_channel_discussion_id)
                ]
                for match_thread, channel_id in match_thread_channel_ids:
                    for force_fetch in (False, True):
                        match_thread_ctx = await please_give_me_the_thread(
                            ctx=self.bot,
                            thread_id=match_thread.id,
                            force_fetch=force_fetch,
                            channel=await please_give_me_the_channel(
                                ctx=self.bot,
                                channel_id=channel_id,
                                force_fetch=force_fetch
                            )
                        )
                        try:
                            await self.process_thread_heartbeating(
                                match_thread,
                                match_thread_ctx,
                                sanity_offset=timedelta(seconds=self.thread_heartbeating * 0.5),
                                force_fetch=force_fetch)
                            break
                        except discord.errors.NotFound:
                            if force_fetch:
                                raise

                    await session.merge(match_thread)

            await session.commit()

    async def send_reminder(self, discussion_thread: Thread, td: timedelta, user_ids: set[int]):
        hours, remainder = divmod(td.total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)
        hours = int(hours)
        minutes = int(minutes)
        if hours == 0 and minutes == 0:
            time_str = 'Match has started'
        else:
            time_str = 'less than '
            if hours != 0:
                time_str += f'{hours}h '
            if minutes != 0:
                time_str += f'{minutes}m '
            time_str += 'is left.'

        embed = discord.Embed()
        embed.title = 'Match Reminder'
        embed.description = time_str
        await discussion_thread.send(content=' '.join(f'<@{id_}>' for id_ in user_ids), embed=embed)

    start_reminder_times: list[timedelta] = [
        timedelta(hours=24),
        timedelta(hours=12),
        timedelta(hours=3),
        timedelta(hours=1),
        timedelta(hours=0)
    ]

    match_reminder_loop_seconds = 60

    @MyCoggy.loop(seconds=match_reminder_loop_seconds)
    async def match_reminder(self):
        if not self.bot.is_ready():
            return

        from blululu.tables import async_engine, Match, MatchDiscussionThread, MatchProposal
        from sqlmodel import select
        # we cant trust the structure of self.start_reminder_times to be sorted so i put this here just in case.
        start_reminder_times_sorted = sorted(self.start_reminder_times, reverse=True)
        sanity_offset = min(start_reminder_times_sorted) - timedelta(seconds=self.match_reminder_loop_seconds*2)
        current_time = datetime.now().astimezone(tz=timezone.utc)
        logger.debug(f"{(current_time + sanity_offset)=}")

        async with AsyncSession(async_engine) as session:
            statement = select(MatchProposal).where(
                MatchProposal.is_confirmed == true(),
                MatchProposal.start_date >= (current_time + sanity_offset)
            )
            match_proposals: list[MatchProposal] = (await session.exec(statement)).all()
            logger.debug(f"match reminders {match_proposals=}")
            # TODO: clean this miss
            for mp in match_proposals:
                start_diff = mp.start_date - current_time
                reminder_diff = None if mp.last_reminder is None else mp.start_date - mp.last_reminder
                # logger.debug(f"{mp=}")
                # logger.debug(f"{start_diff=}, {reminder_diff=}")
                for srt_1, srt_0 in zip(
                        start_reminder_times_sorted,
                        start_reminder_times_sorted[1:] + [sanity_offset]
                ):
                    # logger.debug(f"{srt_0=}, {srt_1=}")
                    # logger.debug(f"{(srt_0 < start_diff < srt_1)=},
                    # {(reminder_diff is None or not srt_0 < reminder_diff < srt_1)=}")
                    start_diff_is_in = srt_0 < start_diff < srt_1
                    reminder_diff_is_in = reminder_diff is None or not srt_0 < reminder_diff < srt_1
                    if start_diff_is_in and reminder_diff_is_in:
                        match: Match = await session.run_sync(lambda _s: mp.match)
                        registered_team: RegisteredTeam = await session.run_sync(lambda _s: match.proposerer_team)
                        match_discussion_thread: MatchDiscussionThread = await session.run_sync(
                            lambda _s: match.match_discussion_thread
                        )
                        timeslots: list[TimeSlot] = await session.run_sync(lambda _s: mp.timeslots)
                        user_ids: set[int] = set()
                        for ts in timeslots:
                            user_ids.add(ts.user_id)
                        discussion_thread = await please_give_me_the_thread(
                            ctx=self.bot,
                            thread_id=match_discussion_thread.id,
                            channel=await please_give_me_the_channel(
                                ctx=self.bot,
                                channel_id=registered_team.discord_channel_discussion_id
                            )
                        )

                        await self.send_reminder(
                            discussion_thread=discussion_thread,
                            td=srt_1,
                            user_ids=user_ids
                        )

                        mp.last_reminder = current_time
                        await session.merge(mp)

                    # here means we already have done the reminder so no point in checking the next
                    elif start_diff_is_in and not reminder_diff_is_in:
                        break

            await session.commit()


class SlashAuthorizationException(SlashCommandException):
    pass


def check_admin(user: Member, guild: Guild) -> bool:
    return user.guild_permissions.administrator


def only_admin(func: Callable) -> Callable:
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        for c in list(args) + list(kwargs.values()):
            if isinstance(c, (SlashContext, ComponentContext, MenuContext)):
                ctx = c
                break
        else:
            raise TypeError("Could not find the context in the function, make sure the function contain a discord "
                            "context")

        if check_admin(user=ctx.author, guild=ctx.guild):
            return await func(*args, **kwargs)
        else:
            logger.warning(f"this user={ctx.author} tried to use this command '{ctx.command}' in server {ctx.guild_id}")
            raise SlashAuthorizationException(message="Sorry, only server admins can run this command",
                                              hidden=True)

    return wrapper