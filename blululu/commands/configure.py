import re
from contextlib import asynccontextmanager, AbstractAsyncContextManager
from datetime import datetime, timedelta, timezone
from pprint import pprint
from typing import Optional, Union, TypedDict, AsyncContextManager, AsyncIterator
from urllib.parse import urlparse
from blululu.logger import logger
import aiohttp
import discord
import sqlalchemy.exc
from discord import Role, TextChannel, PermissionOverwrite, Member
from discord_slash import SlashCommand, SlashContext, SlashCommandOptionType
from discord_slash.utils.manage_commands import create_option, create_choice

from blululu.commands._utils import MyCoggy, please_give_me_the_channel, HelpMessage, HelpMessageCommandExample, \
    only_admin, check_admin, SlashAuthorizationException
from blululu.config import debug_guilds
from blululu.erros import SlashCommandException
from blululu.tables import Team, RegisteredTeam, Games, TimeSlot

TIMEZONES: dict[str, float] = dict(
    AEDT=11,
    AEST=10,
    AWST=8,
    SGT=8,
    AST=3,
    EET=2,
    CET=1,
    UTC=0,
    EST=-5,
    PST=-7,
)


class ConfigUser(MyCoggy):
    config_user_help = HelpMessage(
        description='to configure the user for a specific team',
        examples=[
            HelpMessageCommandExample(
                goal='I would like to configure my self to a team',
                command='/config-user timezone:AEST role:@MyTeamRole'
            ),
            HelpMessageCommandExample(
                goal='I would like to configure someone else not in the team role for try out or whatever',
                command='/config-user timezone:AEST role:@MyTeamRole user:@TheTryoutUserOrWhatever'
            )
        ]
    )

    @MyCoggy.slash(name="config-user", description="configure user",
                   options=[
                       create_option(name="timezone", option_type=3, required=True, description="your timezone",
                                     choices=[
                                         create_choice(name=name, value=str(value)) for name, value in
                                         TIMEZONES.items()]),
                       create_option(name="role", option_type=SlashCommandOptionType.ROLE, required=True,
                                     description="the team role"),
                       create_option(name="user", option_type=SlashCommandOptionType.USER, required=False,
                                     description="to config a user that is not in the team role")
                   ],
                   default_permission=False,
                   command_group='after_config_team',
                   help_message=config_user_help
                   )
    async def config_user(self, ctx: SlashContext, timezone: str, role: Role, user: Optional[Member] = None):
        await ctx.defer(hidden=True)
        logger.debug(f"{timezone=}, {role=}")
        if ctx.author.get_role(role.id) is None:
            raise SlashAuthorizationException(message="you are not authorized to run this command, lol."
                                                      "Make sure you are in the team role or ask someone there to do "
                                                      "the command on your behalf")

        if user is not None:
            if not check_admin(user=ctx.author, guild=ctx.guild):
                raise SlashAuthorizationException(message="only server admin can add another user outside the role")

        from blululu.tables import async_engine, User, RegisteredTeam, UserRegisteredTeamLink
        from sqlmodel import select, func
        from sqlmodel.ext.asyncio.session import AsyncSession

        if external_user := user is not None:
            user = user  # :)
        else:
            user = ctx.author

        user_db = User(id=user.id, timezone=float(timezone))

        async with AsyncSession(async_engine, expire_on_commit=False) as session:

            try:
                team: RegisteredTeam = (await session.exec(
                    select(RegisteredTeam).where(RegisteredTeam.discord_role_id == role.id))  # type: ignore
                                        ).one()
            except sqlalchemy.exc.NoResultFound:
                raise SlashCommandException(message="the team could not be found based on the team role. "
                                                    "maybe the team is not configured, try `/config team` first")

            await session.merge(user_db)
            await session.commit()

            user_team_links: list[UserRegisteredTeamLink] = (await session.exec(
                select(UserRegisteredTeamLink).where(  # type: ignore
                    UserRegisteredTeamLink.team_id == team.id
                )
            )).all()

            if len(user_team_links) >= 10:
                all_users_in_team = [f"<@{u.user_id}>" for u in user_team_links]
                all_users_in_team_str = '\n- '.join(all_users_in_team)
                raise SlashCommandException(
                    message="Sadly, due to complicated design decisions, it is not possible to add more than "
                            "10 players. for now kick one of your players here to accommodate for the new one. maybe "
                            f"you forgot to remove them: \n{all_users_in_team_str}\n to remove players run "
                            f"`/remove-user`"
                )

            link = UserRegisteredTeamLink(team_id=team.id, user_id=user_db.id)
            await session.merge(link)


            any_timeslot: Optional[TimeSlot] = (await session.exec(
                select(TimeSlot).where(TimeSlot.user_id == user.id)  # type: ignore
            )).first()
            if any_timeslot is None:
                logger.debug("new player, will create timeslots")
                # this loops hurts but it is what it is
                timeslots = self.create_timeslots(user_id=user.id)
                for t in timeslots:
                    await session.merge(t)
            await session.commit()

            all_registered_teams: list[RegisteredTeam] = (await session.exec(
                statement=select(RegisteredTeam).join(RegisteredTeam.users).where(  # type: ignore
                    RegisteredTeam.discord_guild_id == ctx.guild_id,
                    User.id == user_db.id
                )
            )).all()
            # assert len(get_all_registered_teams) >= 1
            logger.debug(f"{all_registered_teams=}")
            logger.debug(f"{len(all_registered_teams)=}")

        if external_user:
            match_proposals_channel = await please_give_me_the_channel(
                ctx=ctx,
                channel_id=team.discord_channel_proposals_id
            )

            if match_proposals_channel is None:
                raise ValueError("match_proposals_channel is None, this value should not be None. "
                                 "maybe the bot does not have access to the channel any more")

            match_discussions_channel = await please_give_me_the_channel(
                ctx=ctx,
                channel_id=team.discord_channel_discussion_id
            )

            if match_discussions_channel is None:
                raise ValueError("match_discussion_channel is None, this value should not be None. "
                                 "maybe the bot does not have access to the channel any more")
            user_perm = PermissionOverwrite(read_messages=True, send_messages=False, attach_files=False,
                                add_reactions=False)

            await match_proposals_channel.set_permissions(target=user, overwrite=user_perm)
            await match_discussions_channel.set_permissions(target=user, overwrite=user_perm)

        if len(all_registered_teams) == 1:
            all_guild_command_permissions = await self.get_all_guild_command_permissions(guild_id=ctx.guild_id)
            for command_name in self.command_groups['after_config_user']:
                await self.update_command_permission(
                    command_name=command_name,
                    guild_id=ctx.guild_id,
                    all_guild_command_permissions=all_guild_command_permissions,
                    permissions=[dict(id=str(user.id), type=2, permission=True)],
                    add_permissions=True
                )

        await ctx.send(content=f"configured {user.mention} for the team {role.mention}, "
                               f"now you can `/update-timeslots` adjust your "
                               f"time availability", hidden=True)

    @MyCoggy.slash(name="remove-user", description="remove user",
                   options=[
                       create_option(name="role", option_type=SlashCommandOptionType.ROLE, required=True,
                                     description="the team role"),
                       create_option(name="user", option_type=SlashCommandOptionType.USER, required=False,
                                     description="to remove a user other than you")
                   ],
                   default_permission=False,
                   command_group='after_config_user'
                   )
    @only_admin
    async def remove_user(self, ctx: SlashContext, role: Role, user: Optional[Member] = None):
        await ctx.defer(hidden=True)
        if ctx.author.get_role(role.id) is None:
            raise SlashAuthorizationException(message="you are not authorized to run this command, lol."
                                                      "Make sure you are in the team role or ask someone there to do "
                                                      "the command on your behalf")
        if user is not None:
            if not check_admin(user=ctx.author, guild=ctx.guild):
                raise SlashAuthorizationException(message="only server admin can remove another user")

        if external_user := user is not None:
            user = user  # :)
        else:
            user = ctx.author

        from blululu.tables import async_engine, RegisteredTeam, UserRegisteredTeamLink, User
        from sqlmodel import select
        from sqlmodel.ext.asyncio.session import AsyncSession
        async with AsyncSession(async_engine, expire_on_commit=False) as session:

            try:
                team: RegisteredTeam = (await session.exec(
                    select(RegisteredTeam).where(RegisteredTeam.discord_role_id == role.id)  # type: ignore
                )).one()
            except sqlalchemy.exc.NoResultFound:
                raise SlashCommandException(message="the team could not be found based on the team role. "
                                                    "maybe the team is not configured, try `/config team` first")

            try:
                user_team_link: UserRegisteredTeamLink = (await session.exec(
                    select(UserRegisteredTeamLink).where(  # type: ignore
                        UserRegisteredTeamLink.user_id == user.id, UserRegisteredTeamLink.team_id == team.id)
                )).one()
            except sqlalchemy.exc.NoResultFound:
                raise SlashCommandException(message="the team user could not be found based on the team role. "
                                                    "maybe the user was already deleted or wasn't in the team before.")

            await session.delete(user_team_link)

            await session.commit()
            all_registered_teams: list[RegisteredTeam] = (await session.exec(
                select(RegisteredTeam).join(RegisteredTeam.users).where(  # type: ignore
                    RegisteredTeam.discord_guild_id == ctx.guild_id,
                    User.id == user.id
                )
            )).all()

        external_user = not user.get_role(role.id)

        logger.debug(f"{len(all_registered_teams)=}")
        if len(all_registered_teams) == 0:
            all_guild_command_permissions = await self.get_all_guild_command_permissions(guild_id=ctx.guild_id)
            for command_name in self.command_groups['after_config_user']:
                await self.update_command_permission(
                    command_name=command_name,
                    guild_id=ctx.guild_id,
                    all_guild_command_permissions=all_guild_command_permissions,
                    permissions=[dict(id=str(user.id), type=2, permission=True)],
                    pop_permissions=True
                )

        if external_user:
            match_proposals_channel = await please_give_me_the_channel(
                ctx=ctx,
                channel_id=team.discord_channel_proposals_id
            )

            if match_proposals_channel is None:
                raise ValueError("match_proposals_channel is None, this value should not be None. "
                                 "maybe the bot does not have access to the channel any more")

            match_discussions_channel = await please_give_me_the_channel(
                ctx=ctx,
                channel_id=team.discord_channel_discussion_id
            )

            if match_discussions_channel is None:
                raise ValueError("match_discussion_channel is None, this value should not be None. "
                                 "maybe the bot does not have access to the channel any more")

            await match_proposals_channel.set_permissions(target=user, overwrite=None)
            await match_discussions_channel.set_permissions(target=user, overwrite=None)

            await ctx.send(content=f'the user {user.mention} has been removed from match-proposal channel, '
                                   f'match-discussion channel, and from the bot database for this team. '
                                   'the user can no longer use team specific commands',
                           hidden=True)
        else:
            await user.remove_roles(role)
            await ctx.send(content=f'the user {user.mention} has been removed from the team-role and from the bot '
                                   f'database for this team. the user can no longer use team specific commands',
                           hidden=True)

    @staticmethod
    def create_timeslots(user_id: int) -> list[TimeSlot]:
        current_time = datetime.now(tz=timezone.utc)
        current_time_limited = datetime(year=current_time.year, month=current_time.month, day=current_time.day,
                                        hour=current_time.hour, minute=(current_time.minute // 30) * 30,
                                        tzinfo=current_time.tzinfo)

        rows = []
        for half_hours in range(2 * 24 * 7):
            dt = current_time_limited + timedelta(minutes=30 * half_hours)
            rows.append(TimeSlot(user_id=user_id, date=dt))

        return rows

# TODO: find a better way to typehint json
base_json_type = Union[str, int, float]
_json_type = Union[base_json_type, list[base_json_type], dict[str, base_json_type]]
json_type = Union[_json_type, list[_json_type], dict[str, _json_type]]

class VRMLTeamByName(TypedDict):
    id: str
    name: str
    image: str


class VRMLTeamBio(TypedDict):
    bioinfo: str
    discordServerID: str
    discordInvite: str

# i just filled this with what i needed from https://api.vrmasterleague.com/
class VRMLTeamByID(TypedDict):
    id: str
    name: str
    logo: str
    region: str
    bio: VRMLTeamBio


@asynccontextmanager
async def dummyaio(session: Optional[aiohttp.ClientSession] = None) \
        -> AsyncIterator[aiohttp.ClientSession]:  #AbstractAsyncContextManager[aiohttp.ClientSession]:
    if session is None:
        async with aiohttp.ClientSession() as session_:
            yield session_
    else:
        yield session


async def get_vrml_team_by_name(game: Games, name: str, session: Optional[aiohttp.ClientSession] = None) \
        -> list[VRMLTeamByName]:
    async with dummyaio(session=session) as session_:
        async with session_.get(f"https://api.vrmasterleague.com/{game.name}/Teams/Search",
                               params=dict(name=name)) as resp:
            info = await resp.json()
            logger.debug(f"{info=}")
            if len(info) == 0:
                raise SlashCommandException("check if your team name has any misspelling and try again")

        return info


async def get_vrml_team_by_id(team_id: str, session: Optional[aiohttp.ClientSession] = None) -> VRMLTeamByID:
    async with dummyaio(session=session) as session_:
        # https://api.vrmasterleague.com/
        async with session_.get(f"https://api.vrmasterleague.com/Teams/{team_id}") as resp:
            info = await resp.json()

            formatted_info = VRMLTeamByID(
                id=info['team']['teamID'],
                name=info['team']['teamName'],
                logo=info['team']['teamLogo'],
                region=info['team']['regionName'],
                bio=info['team']['bio']
            )
            return formatted_info


async def get_discord_server_id(game: Games, name: str) -> tuple[Optional[int], VRMLTeamByID]:
    async with aiohttp.ClientSession() as session:
        team_search_info = await get_vrml_team_by_name(game=game, name=name, session=session)
        team_id = team_search_info[0]["id"]
        logger.debug(f"{team_id=}")
        info = await get_vrml_team_by_id(team_id=team_id, session=session)
        logger.debug(info)
        discord_server_id = info['bio']['discordServerID']
        discord_invite = info['bio']['discordInvite']
        logger.debug(f"{discord_server_id=}, {discord_invite=}")
        # team = Team(id=info['id'], name=info['name'], region=info['region'], game=game)

        if discord_server_id is None or len(discord_server_id) == 0:
            domain = urlparse(discord_invite).netloc
            logger.debug(f"{domain=}")
            if discord_invite is not None and domain in ("discord.com", "discord.gg"):

                async with session.get(discord_invite) as resp:
                    html_text = await resp.text()
                    possible_server_ids = set(re.findall(r"https:\/\/cdn.discordapp.com\/\w+\/(\d+)\/", html_text))
                    if len(possible_server_ids) > 0:
                        assert len(possible_server_ids) == 1
                        discord_server_id = list(possible_server_ids)[0]

    if discord_server_id is None or len(discord_server_id) == 0:
        return None, info
    else:
        return int(discord_server_id), info


def convert_vrml_api_team_info_to_row(info: VRMLTeamByID, game: Games) -> Team:
    team = Team(id=info['id'], name=info['name'], region=info['region'], game=game)
    return team


class ConfigTeam(MyCoggy):

    @classmethod
    def get_default_proposal_channel_permission(cls) -> PermissionOverwrite:
        return PermissionOverwrite(read_messages=True, send_messages=False, attach_files=False, add_reactions=False)

    config_team_help = HelpMessage(
        description='to configure the team in the server',
        examples=[
            HelpMessageCommandExample(
                goal='I want to configure my EchoArena team in this server',
                command='/config-team game:EchoArena name:TheBestTeam role:@MyEchoArenaTeamRole '
                        'general_channel:@team-general'
            )
        ]
    )

    @MyCoggy.slash(name="config-team", description="configure team",
                   options=[
                       create_option(name="game", option_type=3, required=True,
                                     description="the game your team plays in",
                                     choices=[
                                         create_choice(name=g.name, value=g.name) for g in Games
                                     ]),
                       create_option(name="name", option_type=str, required=True,
                                     description="your team name"),
                       create_option(name="role", option_type=SlashCommandOptionType.ROLE, required=True,
                                     description="the team role"),
                       create_option(name="general_channel", option_type=SlashCommandOptionType.CHANNEL,
                                     required=True,
                                     description="the team channel")],
                   default_permission=True,
                   help_message=config_team_help
                   )
    @only_admin
    async def config_team(self, ctx: SlashContext, game: str, name: str, role: Role, general_channel: TextChannel):
        await ctx.defer()
        discord_server_id, team_info = await get_discord_server_id(Games[game], name)
        if discord_server_id is None:
            raise SlashCommandException("Make sure your discord server is linked to your vrml team website correctly "
                                        "and the invitation link for your server is still valid")
        team = convert_vrml_api_team_info_to_row(info=team_info, game=Games[game])
        from blululu.tables import async_engine
        from sqlmodel.ext.asyncio.session import AsyncSession
        from sqlmodel import select
        async with AsyncSession(async_engine) as session:
            current_registered_team: Optional[RegisteredTeam]

            current_registered_team = (await session.exec(
                select(RegisteredTeam).where(  # type: ignore
                    RegisteredTeam.id == team.id
                )
            )).first()
            if current_registered_team is not None:
                raise SlashCommandException(message="The team already exist and configured. Maybe the team is "
                                                    "already registered in another discord server.")

            current_registered_team = (await session.exec(
                select(RegisteredTeam).where(  # type: ignore
                    RegisteredTeam.discord_role_id == role.id
                )
            )).first()
            if current_registered_team is not None:
                raise SlashCommandException(message="The team already exist and configured. Are you using the same role"
                                                    " as an already configured team in the same server? maybe you need "
                                                    "to create a new role for the new team.")

            # logger.debug(f"{ctx.guild_id=}")
            # if discord_server_id != ctx.guild_id:
            #     raise SlashCommandException("This server does not match the one in the vrml website, "
            #                                 "make sure you are in the correct server")
            new_channel_permissions = {
                ctx.guild.default_role: PermissionOverwrite(read_messages=False),
                role: PermissionOverwrite(read_messages=True, send_messages=False, attach_files=False,
                                          add_reactions=False),
                ctx.bot.user: PermissionOverwrite(read_messages=True, send_messages=True)
            }
            discussion_channel = await ctx.guild.create_text_channel(
                name="match-discussions",
                category=general_channel.category,
                position=general_channel.position,
                topic=f'channel for discussing matches managed by {ctx.me.mention}',
                overwrites=new_channel_permissions
            )
            proposals_channel = await ctx.guild.create_text_channel(
                name="match-proposals",
                category=general_channel.category,
                position=discussion_channel.position,
                topic=f'channel for proposals matches managed by {ctx.me.mention}',
                overwrites=new_channel_permissions
            )

            all_guild_command_permissions = await self.get_all_guild_command_permissions(guild_id=ctx.guild_id)
            for command_name in self.command_groups['after_config_team']:
                await self.update_command_permission(command_name=command_name,
                                                     guild_id=ctx.guild_id,
                                                     all_guild_command_permissions=all_guild_command_permissions,
                                                     permissions=[dict(id=role.id, type=1, permission=True)],
                                                     add_permissions=True
                                                     )

            logger.debug(f"{discord_server_id=} {role=} {general_channel=} {proposals_channel=}")
            registered_team = RegisteredTeam(discord_guild_id=ctx.guild_id, discord_role_id=role.id,
                                             discord_channel_team_general_id=general_channel.id,
                                             discord_channel_proposals_id=proposals_channel.id,
                                             discord_channel_discussion_id=discussion_channel.id)

            await session.merge(team)
            await session.commit()
            # try:
            registered_team.id = team.id
            session.add(registered_team)
            await session.commit()
            # except sqlalchemy.exc.IntegrityError:
            #     raise SlashCommandException(message="obs, looks like the team is already registered in the bot database "
            #                                     "with another server")

        await ctx.send(content=f"Congrats! now your team {role.mention} is registered, you can `/config-user` to "
                               f"continue the configuration process per player."
                               f" (note: you must be in the {role.mention} role to run that command, otherwise someone"
                               f" in that role can configure an external user by providing the user argument in the"
                               f" `/config-user` command")

    @MyCoggy.slash(name="remove-team", description="remove team",
                   options=[
                       create_option(name="role", option_type=SlashCommandOptionType.ROLE, required=True,
                                     description="the team role")],
                   default_permission=True,
                   command_group='after_config_team'
                   )
    @only_admin
    async def remove_team(self, ctx: SlashContext, role: Role):
        await ctx.defer(hidden=False)
        if ctx.author.get_role(role.id) is None:
            raise SlashCommandException(message="you are not authorized to run this command, lol. Make sure you are in "
                                                "the team role or ask someone there to do the command on your behalf")

        from blululu.tables import async_engine, RegisteredTeam, UserRegisteredTeamLink, MatchProposal, Match
        from sqlmodel import select, true
        from sqlmodel.ext.asyncio.session import AsyncSession
        async with AsyncSession(async_engine, expire_on_commit=False) as session:
            try:
                registered_team: RegisteredTeam = (await session.exec(
                    select(RegisteredTeam).where(RegisteredTeam.discord_role_id == role.id)  # type: ignore
                )).one()
            except sqlalchemy.exc.NoResultFound:
                raise SlashCommandException(message="your team does not exist in the database to be deleted.")

            team: Team = await session.run_sync(lambda _s: registered_team.team)
            match_proposals_channel = await please_give_me_the_channel(
                ctx=ctx,
                channel_id=registered_team.discord_channel_proposals_id
            )

            match_discussions_channel = await please_give_me_the_channel(
                ctx=ctx,
                channel_id=registered_team.discord_channel_discussion_id
            )

            # TODO: figure out a faster way to do the thing bellow with update directly

            match_proposals: list[MatchProposal] = (await session.exec(
                select(MatchProposal).join(Match).where(  # type: ignore
                    Match.team_id == team.id,
                    MatchProposal.start_date >= datetime.now().astimezone(timezone.utc),
                    MatchProposal.is_confirmed == true()
                )
            )).all()
            logger.debug(f"{match_proposals=}")
            for mp in match_proposals:
                mp.is_confirmed = False
                await session.merge(mp)

            await session.commit()

            await session.delete(registered_team)
            await session.commit()

            if match_proposals_channel is not None:
                await match_proposals_channel.delete()

            if match_discussions_channel is not None:
                await match_discussions_channel.delete()

        all_guild_command_permissions = await self.get_all_guild_command_permissions(guild_id=ctx.guild_id)
        for command_name in self.command_groups['after_config_team']:
            await self.update_command_permission(command_name=command_name,
                                                 guild_id=ctx.guild_id,
                                                 all_guild_command_permissions=all_guild_command_permissions,
                                                 permissions=[dict(id=role.id, type=1, permission=True)],
                                                 pop_permissions=True
                                                 )

        await ctx.send(f"team {role.mention} has been deleted. everything now is gone : )")


def setup(slash: SlashCommand, bot: discord.Client):
    ConfigUser().setup(slash=slash, bot=bot)
    ConfigTeam().setup(slash=slash, bot=bot)
