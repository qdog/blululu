import discord
from discord_slash import SlashCommand
from blululu.logger import logger
from blululu.commands._utils import MyCoggy, please_give_me_the_channel

CHECK_ANNOUNCEMENTS_LOOP_DURATION_SECONDS = 60


class Announcements(MyCoggy):

    @MyCoggy.loop(seconds=CHECK_ANNOUNCEMENTS_LOOP_DURATION_SECONDS)
    async def check_announcements(self):
        if not self.bot.is_ready():
            return

        from blululu.tables import async_engine, AdminAnnouncement, RegisteredTeam
        from sqlmodel import select, false
        from sqlmodel.ext.asyncio.session import AsyncSession

        async with AsyncSession(async_engine) as session:
            announcements: list[AdminAnnouncement] = (await session.exec(
                select(AdminAnnouncement).where(AdminAnnouncement.sent == false())  # type: ignore
            )).all()
            if len(announcements) == 0:
                return
            registered_teams: list[RegisteredTeam] = (await session.exec(
                select(RegisteredTeam)  # type: ignore
            )).all()
            for an in announcements:
                logger.info(f"this announcement will be made {an}")
                for rt in registered_teams:
                    team_general = await please_give_me_the_channel(
                        ctx=self.bot,
                        channel_id=rt.discord_channel_team_general_id,
                    )
                    await team_general.send(**an.message)
                an.sent = True
                session.add(an)
            await session.commit()

    @staticmethod
    def send_lie(content: str,
                 embed: discord.Embed = None,
                 ) -> dict:
        embed = embed.to_dict() if embed is not None else None
        return dict(content=content, embed=embed)


def setup(slash: SlashCommand, bot: discord.Client):
    Announcements().setup(slash=slash, bot=bot)


def send_announcement(message: str):
    from blululu.tables import engine, AdminAnnouncement
    from sqlmodel import Session
    from time import sleep, time
    t1 = time()
    with Session(engine) as session:
        an = AdminAnnouncement(message=Announcements.send_lie(content=message))
        session.add(an)
        session.commit()

        while not an.sent:
            session.refresh(an)
            sleep(1)
            if time()-t1 > CHECK_ANNOUNCEMENTS_LOOP_DURATION_SECONDS*1.5:
                raise TimeoutError('The bot did not announce the announcement')
