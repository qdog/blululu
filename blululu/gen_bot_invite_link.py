from enum import Enum


class Permissions(int, Enum):
    # https://discord.com/developers/docs/topics/permissions#permissions-bitwise-permission-flags
    CREATE_INSTANT_INVITE       = 1 << 0
    KICK_MEMBERS                = 1 << 1
    BAN_MEMBERS                 = 1 << 2
    ADMINISTRATOR               = 1 << 3
    MANAGE_CHANNELS             = 1 << 4
    MANAGE_GUILD                = 1 << 5
    ADD_REACTIONS               = 1 << 6
    VIEW_AUDIT_LOG              = 1 << 7
    PRIORITY_SPEAKER            = 1 << 8
    STREAM                      = 1 << 9
    VIEW_CHANNEL                = 1 << 10
    SEND_MESSAGES               = 1 << 11
    SEND_TTS_MESSAGES           = 1 << 12
    MANAGE_MESSAGES             = 1 << 13
    EMBED_LINKS                 = 1 << 14
    ATTACH_FILES                = 1 << 15
    READ_MESSAGE_HISTORY        = 1 << 16
    MENTION_EVERYONE            = 1 << 17
    USE_EXTERNAL_EMOJIS         = 1 << 18
    VIEW_GUILD_INSIGHTS         = 1 << 19
    CONNECT                     = 1 << 20
    SPEAK                       = 1 << 21
    MUTE_MEMBERS                = 1 << 22
    DEAFEN_MEMBERS              = 1 << 23
    MOVE_MEMBERS                = 1 << 24
    USE_VAD                     = 1 << 25
    CHANGE_NICKNAME             = 1 << 26
    MANAGE_NICKNAMES            = 1 << 27
    MANAGE_ROLES                = 1 << 28
    MANAGE_WEBHOOKS             = 1 << 29
    MANAGE_EMOJIS_AND_STICKERS  = 1 << 30
    USE_APPLICATION_COMMANDS    = 1 << 31
    REQUEST_TO_SPEAK            = 1 << 32
    MANAGE_EVENTS               = 1 << 33
    MANAGE_THREADS              = 1 << 34
    CREATE_PUBLIC_THREADS       = 1 << 35
    CREATE_PRIVATE_THREADS      = 1 << 36
    USE_EXTERNAL_STICKERS       = 1 << 37
    SEND_MESSAGES_IN_THREADS    = 1 << 38
    START_EMBEDDED_ACTIVITIES   = 1 << 39
    MODERATE_MEMBERS            = 1 << 40


def perms(*perm: Permissions) -> int:
    if len(perm) == 0:
        raise ValueError("at least provide some permissions")
    starter = -1
    for i, p in enumerate(perm):
        if i == 0:
            starter = p | 0  # to remove the enum stuff
        else:
            starter |= p
    assert starter != -1
    return starter


def print_links(application_id: int):

    administrator = perms(
        Permissions.ADMINISTRATOR
    )
    hardened_permissions = perms(
        Permissions.MANAGE_ROLES,  # to add and remove players when doing `/remove user` `/remove team`
        Permissions.MANAGE_THREADS,  # to create the threads needed in `/propose`
        Permissions.MANAGE_CHANNELS,  # to create the channel for proposals and discussion channels
        Permissions.READ_MESSAGE_HISTORY,  # to delete the heartbeat message in the threads
        Permissions.SEND_MESSAGES,  # to send reminders about matches
        Permissions.CREATE_PUBLIC_THREADS,  # to create match threads
        Permissions.SEND_MESSAGES_IN_THREADS,  # to send messages in the threads for the matches
        Permissions.MANAGE_MESSAGES,  # i dont know but better safe than sorry
        Permissions.EMBED_LINKS,  # to include the link for this project and for other teams website
        Permissions.ATTACH_FILES,  # to attach images for help messages.
        Permissions.USE_APPLICATION_COMMANDS,  # to let the bot create slash commands in the guild
    )

    url = f"https://discord.com/api/oauth2/authorize?client_id={application_id}" \
          "&permissions={permissions}&scope=applications.commands%20bot"
    print("administrator: ", url.format(permissions=administrator))
    print("hardened permissions: ", url.format(permissions=hardened_permissions))
