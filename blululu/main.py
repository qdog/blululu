import os

from blululu.gen_bot_invite_link import print_links
from blululu.announcement import send_announcement
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-b', '--bot-token', type=str, dest='bot_token')
parser.add_argument('-a', '--application-id', type=int, dest='application_id')
parser.add_argument('-l', '--link', action='store_true', dest='link')
parser.add_argument('-m', '--announcement-message', type=str, dest='announcement_message')
args = parser.parse_args()


def main():
    if args.announcement_message is not None:
        send_announcement(message=args.announcement_message)
        return
    else:
        if args.application_id is not None:
            application_id = args.application_id
        elif 'BLULULU_APPLICATION_ID' in os.environ:
            application_id = os.environ['BLULULU_APPLICATION_ID']
        else:
            parser.error('please provide either --application-id as a cli argument or create BLULULU_APPLICATION_ID '
                         'environment variable and set your discord bot application id there.')
            return

        if args.bot_token is not None:
            bot_token = args.bot_token
        elif 'BLULULU_BOT_TOKEN' in os.environ:
            bot_token = os.environ['BLULULU_BOT_TOKEN']
        else:
            parser.error('please provide either --bot-token as a cli argument or create BLULULU_BOT_TOKEN '
                         'environment variable and set your discord bot token there.')
            return

        if args.link:
            print_links(application_id=application_id)
            return

    from blululu.logger import logger
    logger.info("starting BLULULU")
    from blululu.config import version, data_version
    from blululu.update import update
    from blululu.init_bot import slash, bot
    if version != data_version:
        logger.info("not matching versions, will update")
        update(slash=slash, bot=bot)

    from blululu.commands._utils import MyCoggy
    from blululu.commands import updatetimeslots, configure, propose, about
    from blululu import announcement

    logger.info("setting up updatetimeslots")
    updatetimeslots.setup(slash=slash, bot=bot)

    logger.info("setting up configure")
    configure.setup(slash=slash, bot=bot)

    logger.info("setting up propose")
    propose.setup(slash=slash, bot=bot)

    logger.info("setting up about")
    about.setup(slash=slash, bot=bot)

    logger.info("setting up announcement")
    announcement.setup(slash=slash, bot=bot)

    logger.info("create help messages")
    MyCoggy.create_help_message(slash=slash, bot=bot)
    bot.run(bot_token)
