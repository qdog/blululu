# Blulu.lu
A helper bot for teams in [VRML](vrmasterleague.com/) in discord.

# Run
download the source code via
```shell
git clone https://gitlab.com/qdog/blululu.git
```
or download the zip thingy from the gitlab repo then unzip it

in the `data` folder edit the `blululu.yml` file and fill out the info or leave the default value.
note that the `data` folder will contain the bot state and information and the paths in the yaml file is relative to it.

the entry point for the bot is `run.py` and you need to supply the `--bot-token` and `--application-id` for it

## Docker

### took an already built container

you can do
```shell
docker run --rm -v LOCAL_DATA_PATH:/app/data -it registry.gitlab.com/qdog/blululu/blululu:latest
```
where `LOCAL_DATA_PATH` is a directory that the application will store its info such as database, configuration files, 
and version details.

### build it internally
first build the image with
```shell
./build.sh
```

then to run the application simply run
```shell
./run.sh --bot-token $MY_BOT_TOKEN --application-id $MY_APPLICATION_ID
```
note that the default path for the data volume is in the local directory in the `./data` folder, be sure to keep that 
folder safe since the database of the app will be stored there

## Pipenv
if Docker is too much to operate then you can use `pipenv`. 
make sure you first have `pipenv` installed in your system.
```shell
pipenv shell
```

then run the bot
```shell
python run.py --bot-token $MY_BOT_TOKEN --application-id $MY_APPLICATION_ID
```

# Generate discord invite link

## Pipenv
```shell
pipenv run python run.py --bot-token $MY_BOT_TOKEN --application-id $MY_APPLICATION_ID --link
```

## Docker
```shell
./run.sh --bot-token $MY_BOT_TOKEN --application-id $MY_APPLICATION_ID --link
```

# Note
I'm not proud of anything written hear. I'm just testing stuff and playing around. 
it is simply a means to an end.
most likely I will refactor all of it later since I started the project with unstable discord API library.


## Fork info
if you will fork the project, keep in mind that `.gitlab-ci.yml` will run jobs such as for testing, building image, 
running terraform and deployment stuff which might not work if you dont have the CI/CD variables set in the settings 
of the repo. In any case, if you need help forking this project create an issue.

### Terraform stuff
the directory `./terraform` contains an already working setup for creating a simple linode debian instance with external
storage independent of the instance, so even if the instance is gone, the data is still in the external storage.
in terms of costs it maybe around $6-7 dollars? idk. The terraform state is stored in gitlab. 
