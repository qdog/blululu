# v0.0.3
## Bug Fixes
* the 24 hour message reminder showing the match has started [!20](https://gitlab.com/qdog/blululu/-/merge_requests/20)
* the admin check in /config-user and /remove-user is applied incorrectly [!19](https://gitlab.com/qdog/blululu/-/merge_requests/19)

# v0.0.2
## Bug Fixes
* add a reminder 12 hours before the match [!16](https://gitlab.com/qdog/blululu/-/merge_requests/16)
* permission issue when running the command [!15](https://gitlab.com/qdog/blululu/-/merge_requests/15)

# v0.0.1
## Bug Fixes
* wrong error message when proposing a match with a non registered role. [!11](https://gitlab.com/qdog/blululu/-/merge_requests/11)
* the timeslots encoding (encode-timeslots) interfers with discord text formatting [!10](https://gitlab.com/qdog/blululu/-/merge_requests/10)
* bug when the condition string is empty in /view-timeslots and /propose [!8](https://gitlab.com/qdog/blululu/-/merge_requests/8)
* only allow server admins to run /remove-user command [!7](https://gitlab.com/qdog/blululu/-/merge_requests/7)
* proposing a match with no timeslots on the stack doesnt show any error. [!6](https://gitlab.com/qdog/blululu/-/merge_requests/6)

# v0.0.0
## Bug Fixes
* make CHANGELOG.md file edited automatically [!1](https://gitlab.com/qdog/blululu/-/merge_requests/1)
