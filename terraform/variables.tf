variable "linode_token" {
    description = "the api token for linode"
    type = string
    sensitive = true
}

variable "region" {
    description = "The region to deploy the bot and its stuff"
    default = "ap-south"
    type = string
}

variable "root_authorized_key" {
    description = "The ssh public key to use to access the whole instance"
    type = string
    sensitive = true
}

variable "blululu_authorized_key" {
    description = "The ssh public key that is only allowed to access blululu account"
    type = string
    sensitive = true
}

variable "root_pass" {
    description = "the root password dah"
    type = string
    sensitive = true
}

variable "blululu_pass" {
    description = "the password for the user blululu"
    type = string
    sensitive = true
}

variable "blululu_volume_label" {
    description = "the label for the blululu volume"
    type = string
    default = "blululu_volume"
}
