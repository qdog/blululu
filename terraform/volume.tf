resource "linode_volume" "blululu-volume" {
    label = var.blululu_volume_label
    region = var.region
    linode_id = linode_instance.blululu-instance.id
    size = 10
    lifecycle {
        prevent_destroy = false
    }
}

