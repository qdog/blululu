resource "linode_stackscript" "blululuscript" {
    label = "blululu-script"
    description = "Create necessary stuff for the image"
    script = <<EOF
#!/bin/sh
# <UDF name="user_password" label="The password for the user">
# <UDF name="blululu_authorized_key" label="The password for the user">
# <UDF name="blululu_volume" label="The volume for blululu data">
# <UDF name="blululu_volume_label" label="The label for the volume for blululu data">
exec >/root/SSout 2>/root/SSerr

# && sed -i 's/#ChallengeResponseAuthentication/ChallengeResponseAuthentication/g' /etc/ssh/sshd_config \
# && sed -i 's/ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/g' /etc/ssh/sshd_config \
# && sed -i 's/#PasswordAuthentication/PasswordAuthentication/g' /etc/ssh/sshd_config \
# && sed -i 's/#PermitRootLogin/PermitRootLogin/g' /etc/ssh/sshd_config \

echo 'unset HISTFILE' >> /etc/profile.d/disable.history.sh \
&& sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config \
&& sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config \
&& apt-get update -y \
&& apt-get install -y doas \
&& apt-get install -y tmux \
&& echo "permit blululu as root" >> /etc/doas.conf \
&& useradd -m -s /bin/bash blululu \
&& (echo $USER_PASSWORD; echo $USER_PASSWORD) | passwd blululu \
&& mkdir /home/blululu/.ssh \
&& chmod 700 /home/blululu/.ssh \
&& cp /root/.ssh/authorized_keys /home/blululu/.ssh/authorized_keys \
&& echo $BLULULU_AUTHORIZED_KEY >> /home/blululu/.ssh/authorized_keys \
&& chown 600 /home/blululu/.ssh/authorized_keys \
&& chown -R blululu:blululu /home/blululu/.ssh \
&& ssh-keygen -t rsa -b 2048 -q -f /root/.ssh/id_rsa -N "" \
&& cat /root/.ssh/id_rsa.pub >> /home/blululu/.ssh/authorized_keys \
&& ssh-keyscan localhost >> /root/.ssh/known_hosts \
&& service ssh restart \
&& sed -i 's/*) return;;/*) ;;/g' /home/blululu/.bashrc \
&& curl -fsSL https://get.docker.com -o get-docker.sh \
&& sh ./get-docker.sh \
&& usermod -aG docker blululu \
&& apt-get install -y fuse-overlayfs \
&& apt-get install -y uidmap \
&& systemctl disable docker.service \
&& systemctl stop docker.service \
&& rm /var/run/docker.sock \
&& ssh blululu@localhost -t "dockerd-rootless-setuptool.sh install" \
&& ssh blululu@localhost -t "systemctl --user enable docker.service && systemctl --user start docker.service" \
&& printf "export PATH=/usr/bin:$PATH\nexport DOCKER_HOST=unix:///run/user/1000/docker.sock\nalias sudo=doas" >> /home/blululu/.bashrc \
&& apt-get remove -y sudo 

device_name=$(readlink -f $BLULULU_VOLUME | xargs basename)
filesystem_type=$(lsblk -f -o name,fstype | grep $device_name | awk '{print $2}')
[ -z "$filesystem_type" ] && mkfs.ext4 $BLULULU_VOLUME
mkdir /mnt/$BLULULU_VOLUME_LABEL
mount $BLULULU_VOLUME /mnt/$BLULULU_VOLUME_LABEL
chown -R blululu /mnt/$BLULULU_VOLUME_LABEL
echo "$BLULULU_VOLUME   /mnt/$BLULULU_VOLUME_LABEL ext4    defaults 0 2" >> /etc/fstab


EOF
    images = ["linode/debian11"]
    rev_note = "initial version"
}
