terraform {
    required_providers {
        linode = {
            source = "linode/linode"
            version = "1.16.0"
        }
    }
}

provider "linode" {
    token = var.linode_token
}


resource "linode_instance" "blululu-instance" {
        image = "linode/debian11"
        label = "blululu-instance"
        region = var.region
        type = "g6-nanode-1"
        authorized_keys = [var.root_authorized_key]
        root_pass = var.root_pass
        stackscript_id = linode_stackscript.blululuscript.id
        stackscript_data = {
            "user_password" = var.blululu_pass
            "blululu_authorized_key" = var.blululu_authorized_key
            "blululu_volume" = "/dev/disk/by-id/scsi-0Linode_Volume_${var.blululu_volume_label}"
            "blululu_volume_label" = var.blululu_volume_label
        }        
}

output "instance_ip" {
    value = linode_instance.blululu-instance.ip_address
    sensitive = true
}

output "blululu_volume_path" {
  value = "/mnt/${var.blululu_volume_label}"
}